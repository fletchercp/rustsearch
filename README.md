# RustSearch

A document search engine in Rust!

This application is meant to perform functions similar to Elasticsearch.

**This is not production-ready. DO NOT use it in production.** 

## Table of Contents

- [Indexing](#indexing)

## Indexing

Rustsearch implements a simple inverted index using SQLite.

### Definitions

- **Document**: A discrete collection of `Fields` that is to be indexed. A Document is what is submitted to Rustsearch, and its content is indexed.
- **Index**: A collection of Documents.
- **Field**: A field is a key in a Document that has one or more terms as its value.
- **Term**: A term is an exact value that is indexed. "quick", "brown", "Brown" are all examples of distinct terms.
- **Shard**: Documents are stored in Shards, with each Shard being a discrete SQLite database on a host. A shard may be writable (a Primary shard) or read-only (a Replica shard). Writes to an Index are distributed amongst Primary shards. Each Shard has a configurable number of Replica shards.
- **Node**: An instance of RustSearch running on a Host. A Node has a combination of address and port number that must be unique. Multiple Nodes can be running on the same Host, as long as they use a different port.

### Document
A Document is a JSON object that is analyzed and made searchable by RustSearch. A simple example is:

```json
{
    "name": "Perceval Grimwhisker",
    "species": "Cat",
    "age": 3
}
```

For each `Field`, the `Value` may be a `String`, an `Integer` or a `Floating Point` value.

#### IDs
Each `Document` is automatically assigned an ID when it is ingested. This ID is a randomly-generated UUID. There is no need to include an ID with the `Document` submission.

#### Timestamp
A `Document` may include a field named `timestamp`, which must be an ISO 8601 formatted timestamp. If one is not supplied, it is added when the `Document` is processed, and is set to the time when the `Document` was processed.

### Index
An Index is a logical collection of Documents, and is the logical unit for searches. Behind the scenes, an Index is composed of multiple Shards. RustSearch coordinates reads and writes to the appropriate Shards that underly an Index.

### Shard
At the lowest level, a Shard is a SQLite database that resides on a `Host`. The `Node` running on the `Host` reads and writes to this SQLite DB.

#### Primary
An Index can have any number of Primary Shards, distributed amongst various `Nodes`. Primary Shards are the Shard type that accepts writes. When a `Document` is received, a Primary `Shard` for the destination `Index` is chosen, and the `Document` is sent to that `Node` to be written. 

#### Replica
Every `Primary` `Shard` has 0 or more Replicas. These `Replica` `Shards` mirror the data of the `Primary` `Shard`. 