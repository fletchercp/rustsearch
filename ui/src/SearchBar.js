import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
    marginLeft: 100,
    marginRight: 100,
    marginTop: 25,
    padding: 10,
}

const searchFieldStyle = {
    width: '80%',
}

const searchButtonStyle = {
    float: 'right',
    width: '10%',
    marginTop: 5,
}

class SearchBar extends Component {
    handleClick() {
        console.log("Clicked");
    };

    render() {
        return (
            <div>
            <Paper style={style}>
                <TextField
                hintText="Enter your search query..."
                style={searchFieldStyle}
                />
                <RaisedButton label="Search" primary={true} style={searchButtonStyle} onClick={(e) => this.handleClick(e)} />

            </Paper>
            </div>
        )
    }
}
export default SearchBar;
