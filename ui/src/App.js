import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SearchBar from './SearchBar.js';
import LeftMenu from './LeftMenu.js';

class App extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      showIndexTable: false,
    };
  }

  render() {
    return (
      <MuiThemeProvider>
        <LeftMenu />
        <SearchBar />
      </MuiThemeProvider>
    );
  }
}

export default App;
