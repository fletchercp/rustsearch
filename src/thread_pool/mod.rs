use std::collections::HashMap;
use std::thread;
use std::sync::{Mutex, Arc, mpsc};

use inverted_index::document::Document;
use inverted_index::index_sqlite::SQLiteIndex;
use inverted_index::index_store::Index;
type PHashMap = Arc<Mutex<HashMap<String, Arc<Mutex<SQLiteIndex>>>>>;
/// Contains a pool of threads that can receive jobs to handle in the background
pub struct ThreadPool {
    threads: Vec<Worker>,
    sender: Arc<Mutex<mpsc::Sender<IndexJob>>>,
    indices: PHashMap,
}

#[derive(Debug)]
pub struct IndexJob {
    index: String,
    document: Document,
    data_directory: String,
}

impl IndexJob {
    pub fn new(document: Document, index: String, data_directory: String) -> IndexJob {
        IndexJob {
            document: document,
            index: index,
            data_directory: data_directory,
        }
    }
}

impl ThreadPool {
    pub fn new(workers: usize) -> ThreadPool {
        let mut threads = Vec::with_capacity(workers);
        let (sender, receiver) = mpsc::channel();
        let sender = Arc::new(Mutex::new(sender));
        let receiver = Arc::new(Mutex::new(receiver));
        let indices: PHashMap = Arc::new(Mutex::new(HashMap::new()));
        
        for id in 0..workers {
            threads.push(Worker::new(id, Arc::clone(&receiver), Arc::clone(&indices)))
        }

        ThreadPool {
            threads: threads,
            sender: sender,
            indices: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub fn execute(&mut self, job: IndexJob) {
        self.sender.lock().unwrap().send(job).unwrap();
    }
}

struct Worker {
    id: usize,
    thread: thread::JoinHandle<()>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<IndexJob>>>, indices: PHashMap) -> Worker {
        let thread = thread::spawn(move || loop {
            let job = receiver.lock().unwrap().recv().unwrap();
            match indices.lock() {
                Ok(mut l) => {
                    if !l.contains_key(&job.index) {
                        match SQLiteIndex::new(&job.index, &job.data_directory) {
                            Ok(idx) => {
                                l.insert(job.index.clone(), Arc::new(Mutex::new(idx)));
                            },
                            Err(_) => {},
                        };
                    }

                    let idx = l.get(&job.index).unwrap();
                    match idx.lock() {
                        Ok(mut index_lock) => {
                            let _result = index_lock.insert_document(job.document);
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            };
        });

        Worker { id, thread }
    }
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}