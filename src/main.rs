#![allow(dead_code)]
#![recursion_limit = "1024"]

extern crate hyper;
extern crate futures;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate rand;
extern crate uuid;
extern crate regex;
#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate clap;
extern crate rusqlite;
extern crate sys_info;

// System imports
use std::thread;
use std::time;
use std::fs;
use std::thread::sleep;
use std::process::exit;
use std::sync::{Arc, RwLock, mpsc};
// Third party imports
use clap::App;
use rusqlite::Connection;
use uuid::Uuid;
use hyper::server::{Http};

// Setup our own modules
mod inverted_index;
mod cluster;
mod database;
mod http_handlers;
mod rpc;
mod thread_pool;
mod constants;
mod search;

use errors_chain::*;


fn main() {
    if let Err(ref e) = run() {
        println!("error: {}", e);

        for e in e.iter().skip(1) {
            println!("caused by: {}", e);
        }

        // The backtrace is not always generated. Try to run this example
        // with `RUST_BACKTRACE=1`.
        if let Some(backtrace) = e.backtrace() {
            println!("backtrace: {:?}", backtrace);
        }

        ::std::process::exit(1);
    }
}

fn run() -> Result<()> {
    println!("Starting up!");
    let app_config = Arc::new(RwLock::new(parse_cli()));
    let node_manager = Arc::new(RwLock::new(
        cluster::node_manager::NodeManager::new(
            &Arc::clone(&app_config),
        ).unwrap(),
    ));
    let state_tracker = Arc::new(RwLock::new(cluster::trackers::StateTracker::new()));
    let mailbox = Arc::new(RwLock::new(cluster::mailbox::Mailbox::new()));
    let conn = Connection::open(app_config.read().unwrap().cluster_db_path()).expect("Unable to open cluster database");

    let tp = thread_pool::ThreadPool::new(10);

    {
        let mut lock = node_manager.write().unwrap();
        lock.rpc_host = Some(app_config.read().unwrap().rpc_host.to_string());
        lock.rpc_port = Some(app_config.read().unwrap().rpc_port);
    }

    // This creates the channel to receive commands from other Nodes on
    let (tx, rx) = mpsc::sync_channel(1);

    {
        let mut lock = node_manager.write().unwrap();
        lock.tx(tx.clone());
    }

    // Creates the event loop for the the mgmt connections
    let app_config_clone = Arc::clone(&app_config);
    thread::spawn(move || { rpc::listener::listen(&app_config_clone, &tx); });

    // NodeManager is wrapped in an Arc at this point, so we clone it to get another reference
    // to it to use later.
    let nm_clone = Arc::clone(&node_manager);
    let mailbox_clone = Arc::clone(&mailbox);

    // If we are not the master, then we need to try to register with the master
    if !app_config.read().unwrap().is_master {
        println!("We are not master! Starting RPC and beginning registration process...");
        let st_clone = Arc::clone(&state_tracker);
        let app_config_clone = Arc::clone(&app_config);
        thread::spawn(move || {
            rpc::handlers::handle_command(&rx, &st_clone, &nm_clone, &app_config_clone, &mailbox_clone, tp);
        });

        // This needs to be wrapped in a for loop to keep trying until we can register.
        let mut master = cluster::node::NodeInfo::new(
            app_config.read().unwrap().node_id.clone(),
            app_config.read().unwrap().master_host.to_string(),
            app_config.read().unwrap().master_port,
        );
        master.connect();
        let wrapper = rpc::commands::CommandWrapper::new(&Arc::clone(&app_config))
            .command("register_node")
            .to_json();
        match state_tracker.write() {
            Ok(ref mut l) => l.switch(cluster::trackers::NodeState::Registering),
            Err(e) => {
                println!("Unable to lock statetracker: {:?}", e);
                exit(1);
            }
        }
        println!("Registraton request sent!");
        match master.send(wrapper) {
            Ok(_) => {}
            Err(_) => {
                println!("Error sending registration request to master!");
                exit(1);
            }
        };
    } else {
        println!(
            "We are master with id {:?}! Starting RPC on {:?}:{:?}",
            app_config.read().unwrap().node_id.clone(),
            app_config.read().unwrap().rpc_host.clone(),
            app_config.read().unwrap().rpc_port,
        );
        let master = cluster::node::NodeInfo::new(
            app_config.read().unwrap().node_id.clone(),
            app_config.read().unwrap().master_host.clone(),
            app_config.read().unwrap().master_port,
        );
        match master.save_my_id(&conn) {
            Ok(_) => {
                println!("ID saved to db");
            }
            Err(e) => {
                println!("Error saving ID to DB: {:?}", e);
                exit(1);
            }
        };

        match node_manager.write() {
            Ok(ref mut locked) => {
                match locked.add_node(master) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error adding master to node_manager: {:?}", e);
                        exit(1);
                    }
                };
            }
            Err(e) => {
                println!("Error locking node_manager: {:?}", e);
                exit(1);
            }
        };

        // Create the event processing receiver loop
        // StateTracker is wrapped in an Arc, so we clone it to get another reference to pass
        // to the handle_command thread.
        let st_clone = Arc::clone(&state_tracker);
        let mailbox_clone = Arc::clone(&mailbox);
        let app_config_clone = Arc::clone(&app_config);
        thread::spawn(move || {
            rpc::handlers::handle_command(&rx, &st_clone, &nm_clone, &app_config_clone, &mailbox_clone, tp);
        });
    }

    if !app_config.read().unwrap().is_master {
        let one_hundred_millis = time::Duration::from_millis(100);
        loop {
            sleep(one_hundred_millis);
            match state_tracker.write() {
                Ok(ref mut l) => {
                    match l.current_state {
                        cluster::trackers::NodeState::Registered => {
                            println!("Registered with server!");
                            break;
                        }
                        _ => {
                            continue;
                        }

                    };
                }
                Err(_) => {
                    continue;
                }
            };
        }
        // TODO: The GetNodes request should be retried until we can get an answer?
        let wrapper = rpc::commands::CommandWrapper::new(&Arc::clone(&app_config)).command("get_nodes");
        let j = serde_json::to_string(&wrapper).unwrap();
        let mut master = cluster::node::NodeInfo::new(
            "UNKNOWN".to_string(),
            app_config.read().unwrap().master_host.to_string(),
            app_config.read().unwrap().master_port,
        );
        match master.send(j) {
            Ok(_) => {}
            Err(e) => {
                println!("Failed to get nodes from master: {:?}", e);
                exit(1);
            }
        }
    }
    let mut router = http_handlers::router::Router::new();

    router.add_route(r"^/static/", http_handlers::files::handle_route_static);
    router.add_route(r"/ui", http_handlers::files::index);

    router.add_route(r"^/v1/api/healthz", http_handlers::management::healthcheck);
    router.add_route(r"/v1/api/nodes", http_handlers::nodes::list);

    router.add_route(r"/v1/api/index/.+/document", http_handlers::index::document_request);
    router.add_route(r"/v1/api/index", http_handlers::index::handle_route_index);
    router.add_route(r"/v1/api/indices", http_handlers::index::list_indices);
    router.add_route(r"/v1/api/index/.+/shards", http_handlers::index::shards);
    
    router.add_route(r"/v1/api/index/.+/search", http_handlers::index::search_index);

    router.add_route(r"/v1/api/search", http_handlers::search::search);

    router.add_route(r"/v1/api/metrics/summary", http_handlers::metrics::summary);
    
    let addr = "127.0.0.1:3000".parse().unwrap();
    let rust_search = http_handlers::service::RustSearch::new(router, node_manager, mailbox, app_config);
    let server = Http::new().bind(&addr, move || Ok(rust_search.clone())).unwrap();
    server.run().unwrap();
    Ok(())
}

fn parse_cli() -> AppConfig {
    // Reads and parses the config provided at the CLI
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let data_directory = matches.value_of("data_directory").unwrap_or(
        "/tmp/rusticsearch/",
    );
    
    match fs::create_dir_all(data_directory) {
        Ok(_) => {},
        Err(e) => {
            println!("There was an error creating the data directory: {:?}", e);
            exit(1);
        }
    };

    let conn = Connection::open(
        data_directory.to_owned() + constants::DEFAULT_CLUSTER_DB_NAME,
    ).expect("Unable to open cluster database");

    match conn.execute(database::queries::QUERY_CREATE_CONFIGURATION_TABLE, &[]) {
        Ok(_) => {}
        Err(e) => {
            println!("Error creating configuration table: {:?}", e);
            exit(1);
        }
    };

    let node_id = match cluster::node::NodeInfo::load_my_id(&conn) {
        Ok(id) => {
            id
        },
        Err(_) => {
            Uuid::new_v4().to_string()
        }
    };

    let master_host = matches.value_of("master_host").unwrap_or(
        constants::DEFAULT_MASTER_HOST,
    );

    // TODO: Is there a way to parse this as an i64 without the extra step here?
    let master_port = matches.value_of("master_port").unwrap_or(
        constants::DEFAULT_MASTER_PORT,
    );
    let master_port = master_port.parse::<i64>().expect(
        "Unable to parse master port",
    );

    let rpc_host = matches
        .value_of("rpc_addr")
        .unwrap_or(constants::DEFAULT_RPC_HOST)
        .to_string();

    let rpc_port = matches.value_of("rpc_port").unwrap_or(
        constants::DEFAULT_RPC_PORT,
    );
    let rpc_port = rpc_port.parse::<i64>().expect("Unable to parse RPC port");

    let is_master = matches.is_present("master");
    let is_data = matches.is_present("role_data");

    AppConfig {
        node_id: node_id.to_string(),
        data_directory: data_directory.to_string(),
        master_host: master_host.to_string(),
        master_port: master_port,
        rpc_host: rpc_host,
        rpc_port: rpc_port,
        is_master: is_master,
        is_data: is_data
    }
}

/// Struct that contains the application configuration at startup
#[derive(Clone)]
pub struct AppConfig {
    pub node_id: String,
    pub data_directory: String,
    pub master_host: String,
    pub master_port: i64,
    pub rpc_host: String,
    pub rpc_port: i64,
    pub is_master: bool,
    pub is_data: bool
    
}

impl AppConfig {
    pub fn new_from_defaults() -> AppConfig {
        AppConfig {
            node_id: "".to_string(),
            data_directory: "".to_string(),
            master_host: "".to_string(),
            master_port: 0,
            rpc_host: "localhost".to_string(),
            rpc_port: 24443,
            is_master: false,
            is_data: true
        }
    }

    pub fn cluster_db_path(&self) -> String {
        self.data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME
    }
}

mod errors_chain {
    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain! {
        errors {

            SQLQueryFailed(q: String) {
                description("A SQL query failed")
                display("query '{}' failed", q)
            }

            DatabaseError(db: String) {
                description("An error with the database")
                display("Error talking to database '{}'", db)
            }

            ReadAppConfigError(e: String) {
                description("An error getting a read lock on the app config struct")
                display("Unable to lock appconfig for reading")
            }

            NodeSocketFailed(n: String) {
                description("A Node's socket went away")
                display("node '{}' went away", n)
            }

            NodeSendFailed(n: String, e: String) {
                description("A Node's socket went away")
                display("A send to '{}' failed with error '{}'", n, e)
            }

            DocumentAddError(d: String) {
                description("An error with adding a Document")
                display("There was an error adding document '{}'", d)
            }

            DocumentAddBulkCommit(e: String) {
                description("An error with committing the transaction containing multiple Documents")
                display("There was an error adding documents '{}'", e)
            }

            DocumentDeleteError(d: String, e: String) {
                description("An error with deleting a Document")
                display("There was an error deleting document '{}'. Error was: '{}'", d, e)
            }

            IndexCreateError(n: String, e: String) {
                description("An error creating a new index")
                display("There was an error '{}' creating index '{}'", e, n)
            }

            IndexAddTerm(i: String, t: String, e: String) {
                description("An error adding a term to an index")
                display("There was an error adding term '{}' to index '{}'. Error was '{}'", i, t, e)
            }

            IndexAddField(i: String, t: String, e: String) {
                description("An error adding a field to an index")
                display("There was an error adding field '{}' to index '{}'. Error was '{}'", i, t, e)
            }

            IndexAddDocument(i: String, t: String, e: String) {
                description("An error adding a document to an index")
                display("There was an error adding document '{}' to index '{}'. Error was '{}'", i, t, e)
            }

            IndexAddMapping(i: String, t: String, e: String) {
                description("An error adding a mapping to a field in an index")
                display("There was an error adding mapping '{}' for term index '{}'. Error was '{}'", i, t, e)
            }

            IndexStoreDelete(i: String, e: String) {
                description("An error deleting an index store")
                display("There was an error deleteing index store for index '{}'. Error was: '{}'", i, e)
            }

            OccurrenceAdd(t: String, i: String, e: String) {
                description("An error adding an Occurrence")
                display("There was an error adding an occurrence for term '{}' to index '{}'. Error was '{}'", t, i, e)
            }

            OccurrenceDelete(d: String, e: String) {
                description("An error deleting an Occurrence")
                display("There was an error deleting occurrences in document '{}'. Error was '{}'", d, e)
            }

            QueryJsonParser(d: String, e: String) {
                description("An error deleting an Occurrence")
                display("There was an error deleting occurrences in document '{}'. Error was '{}'", d, e)
            }

            NotYetImplemented(s: String) {
                description("An error when trying to use something not yet implemented")
                display("This feature is not yet implemented")
            }

        }
    }
}
