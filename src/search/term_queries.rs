use search::query::{Query,QResult};
use std::collections::{HashSet};
use rusqlite::Connection;
use database::queries::*;

#[derive(Debug)]
/// Term is a field to search and the value to search it for
pub struct Term {
    field: String,
    value: String,
}

impl Term {
    /// Creates and returns a new Term
    pub fn new(field: &str, value: &str) -> Term {
        Term {
            field: field.to_string(),
            value: value.to_string(),
        }
    }
}

impl Query for Term {
    /// Executes a Term Query
    fn execute(&self, conn: &Connection, result: &mut QResult) {
        let mut stmt = conn.prepare(QUERY_DOCUMENTS_WITH_TERM_IN_FIELD).unwrap();
        let iter = stmt.query_map(&[&self.field, &self.value], |row| {
            row.get(0)
        }).unwrap();

        for d in iter {
            result.documents.push(d.unwrap());
        }
    }

    fn display(&self) -> String {
        format!("Field: {:?} Value: {:?}", self.field, self.value)
    }
}

/// Terms is a query for multiple values
#[derive(Debug)]
pub struct Terms {
    field: String,
    values: Vec<String>
}

impl Terms {
    /// Creates and returns a new Terms query
    pub fn new(field: &str, values: Vec<String>) -> Terms {
        Terms {
            field: field.to_string(),
            values: values,
        }
    }
}

impl Query for Terms {
    fn execute(&self, conn: &Connection, _result: &mut QResult) {
        let mut sets: Vec<HashSet<String>> = Vec::new();
        for value in &self.values {
            let mut new_set: HashSet<String> = HashSet::new();
            let mut stmt = conn.prepare(QUERY_DOCUMENTS_WITH_TERM_IN_FIELD).unwrap();
            let iter = stmt.query_map(&[&self.field, value], |row| {
                row.get(0)
            }).unwrap();
            for d in iter {
                new_set.insert(d.unwrap());
            }
            sets.push(new_set);
            if sets.len() == 2 {
                let left  = sets.pop().unwrap();
                let right = sets.pop().unwrap();
                let intersection = left.intersection(&right);
                let mut new = HashSet::new();
                for i in intersection {
                    new.insert(i.to_string());
                }
                sets.push(new);
            }
        }
    }

    fn display(&self) -> String {
        format!("Field: {:?} Values: {:?}", self.field, self.values)
    }
}

/// Range is a query that looks for a field within a certain range of values
#[derive(Debug)]
pub struct Range {
    field: String,
    parameters: Vec<RangeOperatorValues>,
}

#[derive(Debug)]
pub struct RangeOperatorValues {
    pub operator: String,
    pub value: String,
}

impl Range {
    pub fn new(field: &str, parameters: Vec<RangeOperatorValues>) -> Range {
        Range{
            field: field.to_string(),
            parameters: parameters,
        }
    }
}

impl Query for Range {
    fn execute(&self, conn: &Connection, result: &mut QResult) {
        let mut tmp_query_string = QUERY_PARTIAL_RANGE.to_string();
        for p in &self.parameters {
            let tmp_additional_string = format!("AND term {} {} ", p.operator, p.value);
            tmp_query_string += &tmp_additional_string;
            
        }
        let mut stmt = conn.prepare(&tmp_query_string).unwrap();
        let mut docs: Vec<String> = vec![];

        let iter = stmt.query_map(&[&self.field], |row| {
            row.get(0)
        }).unwrap();
        for d in iter {
            docs.push(d.unwrap());
        }
        result.documents = docs;
    }

    fn display(&self) -> String {
        format!("Field: {:?} Parameters: {:?}", self.field, self.parameters)
    }
}

/// Query to check if a field exists at all
pub struct Exists {
    field: String
}

impl Exists {
    pub fn new(field: &str) -> Exists {
        Exists {
            field: field.to_string(),
        }
    }
}

impl Query for Exists {
    /// Executes an Exists Query
    fn execute(&self, conn: &Connection, result: &mut QResult) {
        let mut stmt = conn.prepare(QUERY_TERM_IN_DOCUMENTS).unwrap();
        // TODO: THere has to be a more elegant way to do this
        let iter = stmt.query_map(&[&self.field], |_row| {
            
        }).unwrap();
        for _ in iter {
            result.exists = true;
        }
    }

    /// Displays an Exists query
    fn display(&self) -> String {
        format!("{:?}", self.field)
    }
}

/// Query to check if a field starts with a string
pub struct Prefix {
    field: String,
    prefix: String,
}

impl Prefix {
    pub fn new(field: &str, prefix: &str) -> Prefix {
        Prefix {
            field: field.to_string(),
            prefix: prefix.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use inverted_index::document::Document;
    use inverted_index::index_sqlite::SQLiteIndex;
    use inverted_index::index_store::Index;
    use rusqlite::Connection;
    use search::query::QResult;
    use search::query::Query;
    use search::term_queries::{Term, Terms, Exists, Range, RangeOperatorValues};
    fn generate_app_config() -> ::AppConfig {
        ::AppConfig {
            node_id: "test_id".to_string(),
            data_directory: "/tmp/rusticsearch_test".to_string(),
            master_host: "127.0.0.1".to_string(),
            master_port: 9000,
            rpc_host: "127.0.0.1".to_string(),
            rpc_port: 9001,
            is_master: true,
            is_data: true
        }
    }

    fn generate_test_document() -> Document {
        let mut test_data: HashMap<String, String> = HashMap::new();
        let cookie_string = "chocolate sugar";
        test_data.insert("cookie".to_string(), cookie_string.to_string());
        test_data.insert("age".to_string(), "5".to_string());
        let d = Document::from_fields(test_data);
        return d;
    }

    fn generate_test_document2() -> Document {
        let mut test_data: HashMap<String, String> = HashMap::new();
        let cookie_string = "chocolate oatmeal";
        test_data.insert("cookie".to_string(), cookie_string.to_string());
        let d = Document::from_fields(test_data);
        return d;
    }

    #[test]
    fn test_query_term_range() {
        let mut idx = SQLiteIndex::new("test-range-query-idx", "/tmp/rusticsearch_test").unwrap();
        let doc = generate_test_document();
        let _result = idx.insert_document(doc);
        let rov = vec![RangeOperatorValues{operator: ">".to_string(), value: "2".to_string()}];
        let range_query = Range::new("age", rov);
        let conn = Connection::open("/tmp/rusticsearch_test/test-range-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        range_query.execute(&conn, &mut query_result);
        assert!(query_result.documents.len() > 0);

        let rov = vec![RangeOperatorValues{operator: "<".to_string(), value: "2".to_string()}];
        let range_query = Range::new("age", rov);
        let conn = Connection::open("/tmp/rusticsearch_test/test-range-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        range_query.execute(&conn, &mut query_result);
        assert!(query_result.documents.len() == 0);
    }

    #[test]
    fn test_query_term_exists() {
        let mut idx = SQLiteIndex::new("test-exists-query-idx", "/tmp/rusticsearch_test").unwrap();
        let doc = generate_test_document();
        let _result = idx.insert_document(doc);
        let exists_query = Exists::new("chocolate");
        let conn = Connection::open("/tmp/rusticsearch_test/test-exists-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        exists_query.execute(&conn, &mut query_result);
        assert!(query_result.exists);
    }

    #[test]
    fn test_query_term_matches() {
        let mut idx = SQLiteIndex::new("test-query-idx", "/tmp/rusticsearch_test").unwrap();
        let doc = generate_test_document();
        let _result = idx.insert_document(doc);
        let term_query = Term::new("cookie", "chocolate");
        let conn = Connection::open("/tmp/rusticsearch_test/test-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        let _result = term_query.execute(&conn, &mut query_result);
        let term_query = Term::new("cookie", "oatmeal");
        let _result = term_query.execute(&conn, &mut query_result);
    }

    #[test]
    fn test_query_terms_match() {
        let mut idx = SQLiteIndex::new("test-query-idx", "/tmp/rusticsearch_test").unwrap();
        let doc = generate_test_document();
        let doc2 = generate_test_document2();
        let _result = idx.insert_document(doc);
        let _result = idx.insert_document(doc2);
        let mut q = Vec::new();
        q.push("chocolate".to_string());
        q.push("sugar".to_string());
        let terms_query = Terms::new("cookie", q);
        let conn = Connection::open("/tmp/rusticsearch_test/test-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        let _result = terms_query.execute(&conn, &mut query_result);
    }

    #[test]
    fn test_query_exists() {
        let mut idx = SQLiteIndex::new("test-query-idx", "/tmp/rusticsearch_test").unwrap();
        let doc = generate_test_document();
        let _result = idx.insert_document(doc);
        let exists_query = Exists::new("cookie");
        let conn = Connection::open("/tmp/rusticsearch_test/test-query-idx.db").unwrap();
        let mut query_result = QResult{
            documents: Vec::new(),
            exists: false,
        };
        let _result = exists_query.execute(&conn, &mut query_result);
    }
}
