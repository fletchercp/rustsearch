#[derive(Clone, Debug)]
pub struct Scorecard {
    document_id: String,
    score: f64,
}

impl Scorecard {
    pub fn new(document_id: &str) -> Scorecard {
        Scorecard {
            document_id: document_id.to_string(),
            score: 0.0,
        }
    }
}
