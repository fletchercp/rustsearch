use serde_json;
use errors_chain::*;
use search::term_queries::{Term, RangeOperatorValues, Range};
use rusqlite::Connection;

pub struct Q {
    pub queries: Vec<Box<Query>>
}

impl Q {
    /// Represents a query or queries that are executred against an index
    /// 
    /// # Arguments
    /// 
    /// * `json` - JSON-encoded string from a client that contains the query or queries they wish to execute
    /// 
    pub fn from_json(json: &str) -> Result<Q> {
        let parsed: serde_json::Value;
        let mut result = Q{
            queries: vec![],
        };

        match serde_json::from_str(json) {
            Ok(v) => {
                parsed = v;
                if !parsed.is_object() {
                    return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                }

                
                let parsed = match parsed.as_object().unwrap().get("query") {
                    Some(query) => { query },
                    None => {
                        return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                    }
                };

                // This section checks for the prescence of various keys to determine what type of query it is
                if let Some(term) = parsed.get("term") {
                    let (key, value) = Q::extract_term_info(&term)?;
                    let term_obj = Term::new(&key, &value);
                    result.queries.push(Box::new(term_obj));
                }

                 if let Some(terms) = parsed.get("terms") {
                    let terms = match terms.as_array() {
                        Some(arr) => { arr },
                        None => { return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into()); }
                    };

                    for term in terms {
                        let (key, value) = Q::extract_term_info(&term)?;
                        let term_obj = Term::new(&key, &value);
                        result.queries.push(Box::new(term_obj));
                    }
                }
                
                if let Some(range) = parsed.get("range") {
                    let range = match range.as_object() {
                        Some(range) => { range },
                        None => {
                            return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                         }
                    };

                    let field = match range.get("field") {
                        Some(f) => {
                            match f.as_str() {
                                Some(s) => {
                                    s
                                },
                                None => {
                                    return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                                },
                            }
                        },
                        None => {
                            return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                        },
                    };

                    let parameters = match range.get("parameters") {
                        Some(parameters) => {
                            match parameters.as_array() {
                                Some(parameters) => { parameters },
                                None => {return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());}
                            }
                        },
                        None => {return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());}
                    };
                    
                    let mut parameters_vec: Vec<RangeOperatorValues> = vec![];
                    for p in parameters {
                        match p.as_object() {
                            Some(p) => {
                                let operator = p.get("operator").unwrap();
                                let value = p.get("value").unwrap();
                                parameters_vec.push(
                                    RangeOperatorValues{
                                        operator: operator.as_str().unwrap().to_string(),
                                        value: value.as_str().unwrap().to_string(),
                                    }
                                );
                            },
                            None => {
                                return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
                            }
                        };
                    }
                    result.queries.push(Box::new(Range::new(field, parameters_vec)));
                }
            },
            Err(e) => {
                return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), e.to_string()).into());
            }
        };
        Ok(result)
    }

    /// Extracts the key and value from a Term query
    /// 
    /// # Arguments
    /// * `q` - Reference to a serde_json Value that has been parsed out of a JSON body
    /// 
    fn extract_term_info(q: &serde_json::Value) -> Result<(String, String)> {
        let term = match q.as_object() {
            Some(t) => { 
                t
            },
            None => {
                return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
             }
        };

        let key = match term.keys().nth(0) {
            Some(t) => { 
                t.as_str()
            },
            None => {
                return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
            }
        };

        let value = match term.values().nth(0) {
            Some(t) => { 
                t.as_str().unwrap() 
            },
            None => {
                return Err(ErrorKind::QueryJsonParser("Unable to parse Q".to_string(), "json was invalid".to_string()).into());
            }
        };

        Ok((key.to_string(), value.to_string()))
    }
}

impl Query for Q {
    fn execute(&self, conn: &Connection, result: &mut QResult) {
        for q in &self.queries {
            q.execute(conn, result);
        }
    }

    fn display(&self) -> String {
        let mut result = vec![];
        for q in &self.queries {
            result.push(q.display());
        }
        result.join("\n")
    }
}

#[derive(Debug)]
pub struct QResult {
    pub documents: Vec<String>,
    pub exists: bool,
}

pub trait Query {
    fn execute(&self, d: &Connection, result: &mut QResult);
    fn display(&self) -> String;
}

pub struct QueryRequest {
    queries: Vec<SingleQueryRequest>,
}

pub struct SingleQueryRequest {
    query_type: String,
    query_body: String,
}

#[cfg(test)]
mod tests {
    use super::*;
    fn generate_app_config() -> ::AppConfig {
        ::AppConfig {
            node_id: "test_id".to_string(),
            data_directory: "/tmp/rusticsearch_test".to_string(),
            master_host: "127.0.0.1".to_string(),
            master_port: 9000,
            rpc_host: "127.0.0.1".to_string(),
            rpc_port: 9001,
            is_master: true,
            is_data: true
        }
    }

    #[test]
    fn test_json_to_term() {
        let q = r#"{
                    "query": {
                        "term": { "cookie": "chocolate" }
                    }
                }"#;

        let query = Q::from_json(q);
        assert!(query.is_ok());
        assert!(query.unwrap().queries.len() == 1);
    }

    #[test]
    fn test_json_to_terms() {
        let q = r#"{
                    "query": {
                        "terms": [
                            { "cookie": "chocolate" },
                            { "cookie": "sugar" }
                        ]
                    }
                }"#;

        let query = Q::from_json(q);
        assert!(query.is_ok());
        assert!(query.unwrap().queries.len() > 1);
    }

    #[test]
    fn test_json_to_range() {
        let q = r#"{
                    "query": {
                        "range": {"field": "age", 
                                  "parameters": [
                                        { 
                                          "operator": "gt",
                                          "value": "2" 
                                        }
                                  ] 
                        }
                    }
                }"#;

        let query = Q::from_json(q);
        assert!(query.is_ok());
    }
}
