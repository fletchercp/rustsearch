/// This is the default hostname where we should look for the master
pub static DEFAULT_MASTER_HOST: &'static str = "localhost";
/// This is the default port we should use to try to connect to the master
pub static DEFAULT_MASTER_PORT: &'static str = "24332";
/// This is the default RPC host we should listen on
pub static DEFAULT_RPC_HOST: &'static str = "localhost";
/// This is the default RPC port we should use
pub static DEFAULT_RPC_PORT: &'static str = "24333";
/// This is the name of the database that holds the cluster configuration
pub static DEFAULT_CLUSTER_DB_NAME: &'static str = "cluster.db";
pub static DEFAULT_METRICS_DB_NAME: &'static str = "metrics.db";