use errors_chain::*;
use inverted_index::sqlite_index_store::SQLiteIndexStore;
use inverted_index::occurrence::Occurrence;
use inverted_index::document::Document;
use inverted_index::index_store::{IndexStore, Index};
use inverted_index::splitter;
use inverted_index::occurrence;
use database;
use rusqlite::Connection;
use search::query::Q;
use search::query;

/// Index is an inverted index of terms to documents
pub struct SQLiteIndex {
    name: String,
    data_directory: String,
    pub storage_engine: SQLiteIndexStore,
}

impl SQLiteIndex {
    pub fn new(name: &str, path: &str) -> Result<SQLiteIndex> {
        match SQLiteIndexStore::new(name, path) {
            Ok(s) => {
                Ok(SQLiteIndex {
                    name: name.to_string(),
                    data_directory: path.to_string(),
                    storage_engine: s,
                })
            }
            Err(e) => {
                Err(
                    ErrorKind::IndexCreateError(name.to_string(), e.to_string()).into(),
                )
            }
        }
    }
}

impl Index for SQLiteIndex {
    fn name(&self) -> &str {
        &self.name
    }

    /// This processes a Q (query)
    fn process_query(&self, q: Q) -> Result<query::QResult> {
        let mut result = query::QResult{
            documents: Vec::new(),
            exists: false,
        };
        let conn = Connection::open(self.data_directory.clone() + "/" + &self.name + ".db").unwrap();
        // A Q may be made up of multiple queries, which may in turn contain more child queries
        for query in &q.queries {
            query.execute(&conn, &mut result);
        }
        Ok(result)
    }

    fn insert_document(&mut self, d: Document) -> Result<()> {
        match self.storage_engine.add_document(&d) {
            Ok(_) => {}
            Err(_) => {
                return Err(ErrorKind::DocumentAddError(d.id_as_string()).into());
            }
        };
        for (k, v) in &d.fields {
            
            let terms = splitter::split_on_whitespace(v);
            match self.storage_engine.add_field(k, &d.id_as_string(), v) {
                Ok(_) => {}
                Err(e) => {
                    println!("Error adding field: {:?}", e);
                }
            };
            
            let data_type = database::DataType::from_str(v).unwrap();
            match self.storage_engine.add_mapping(k, &data_type) {
                Ok(_) => {
                    
                },
                Err(e) => {
                    println!("Error adding mapping for field {:?} with data type {:?}. Error was: {:?}.", k, data_type, e.to_string());
                }
            };

            for t in &terms {
                match self.storage_engine.add_term(&t.value) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error adding term: {:?}", e);
                    }
                };

                let new_occurrence = occurrence::Occurrence::new(d.id_as_string().clone(), k.to_string(), t.offset as u32);
                match self.storage_engine.add_occurrence(
                    &t.value,
                    &new_occurrence,
                ) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error adding occurence: {:?}", e);
                    }
                };
            }
        }
        Ok(())
    }

    fn insert_bulk_document(&mut self, documents: Vec<Document>) -> Result<()> {
        let mut conn = Connection::open(self.data_directory.clone() + "/" + &self.name + ".db").unwrap();
        let tx = conn.transaction().unwrap();
        for d in documents {
            match self.storage_engine.add_document(&d) {
                Ok(_) => {}
                Err(e) => {
                    println!("Error adding docoument during bulk insertion: {:?}", e);
                }
            };
        }
        match tx.commit() {
            Ok(_) => Ok(()),
            Err(e) => Err(ErrorKind::DocumentAddBulkCommit(e.to_string()).into()),
        }
    }

    fn document_by_id(&self, document_id: &str) -> Result<Document> {
        self.storage_engine.document_by_id(document_id)
    }

    fn occurrences_by_term(&self, term: &str) -> Result<Vec<Occurrence>> {
        self.storage_engine.occurrences_for_term(term)
    }

    fn documents_with_term_in_field(&self, field: &str, term: &str) -> Result<Vec<String>> {
        self.storage_engine.documents_with_term_in_field(field, term)
    }

    fn delete(&self) -> Result<()> {
        self.storage_engine.delete()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sqlite_index() {
        let idx = SQLiteIndex::new("test-idx", "/tmp");
        assert!(idx.is_ok());
        let mut idx = idx.unwrap();
        let d = Document::new("This is a test document");
        let doc_id = d.id_as_string();
        let result = idx.insert_document(d);
        assert!(result.is_ok());
        let doc_result = idx.document_by_id(&doc_id);
        assert!(doc_result.is_ok());
        let find_result = idx.occurrences_by_term("test");
        assert!(find_result.is_ok());
        let find_result = find_result.unwrap();
        assert_eq!(find_result.len(), 1);
        let delete_result = idx.delete();
        assert!(delete_result.is_ok());
    }

    // #[bench]
    // fn bench_index_document(b: &mut Bencher) {
    //     let mut idx = SQLiteIndex::new("test-idx-bench-1", "/tmp").unwrap();

    //     b.iter(|| {
    //         let d = Document::new("This is a test document");
    //         idx.insert_document(d);
    //     });
    //     idx.delete().expect(
    //         "Error deleting index used for benchmarking",
    //     );
    // }
}
