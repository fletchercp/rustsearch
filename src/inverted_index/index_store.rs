/// This Trait can be implemented by anything that wants to provide a store for the inverted indexes.

use errors_chain::*;
use inverted_index::document::Document;
use inverted_index::occurrence::Occurrence;
use search::query::Q;
use search::query;
use database::DataType;

pub trait IndexStore {
    fn exists(name: &str) -> Result<bool>;
    fn save(&self) -> Result<()>;
    fn add_term(&self, term: &str) -> Result<()>;
    fn add_field(&self, field: &str, document: &str, content: &str) -> Result<()>;
    fn add_mapping(&self, field: &str, data_type: &DataType) -> Result<()>;
    fn terms(&self) -> Result<Vec<String>>;
    fn add_document(&mut self, document: &Document) -> Result<()>;
    fn add_occurrence(&self, term: &str, occurrence: &Occurrence) -> Result<()>;
    fn occurrences_for_term(&self, term: &str) -> Result<Vec<Occurrence>>;
    fn count_terms(&self) -> Result<u32>;
    fn document_by_id(&self, document_id: &str) -> Result<Document>;
    fn delete_document_by_id(&self, document_id: &str) -> Result<()>;
    fn delete_occurrences_by_document_id(&self, document_id: &str) -> Result<()>;
    fn delete(&self) -> Result<()>;
    fn documents_with_term_in_field(&self, field: &str, term: &str) -> Result<Vec<String>>;

}

pub trait Index {
    fn name(&self) -> &str;
    fn insert_document(&mut self, d: Document) -> Result<()>;
    fn document_by_id(&self, document_id: &str) -> Result<Document>;
    fn insert_bulk_document(&mut self, documents: Vec<Document>) -> Result<()>;
    fn occurrences_by_term(&self, term: &str) -> Result<Vec<Occurrence>>;
    fn delete(&self) -> Result<()>;
    fn process_query(&self, q: Q) -> Result<query::QResult>;
    fn documents_with_term_in_field(&self, field: &str, term: &str) -> Result<Vec<String>>;
}
