/// This is an index store that keeps its data in `SQLite` databases
use rusqlite::Connection;
use inverted_index::index_store::IndexStore;
use database::queries::*;
use std::fs::remove_file;
use std::collections::HashMap;

use inverted_index::document::Document;
use inverted_index::occurrence::Occurrence;
use database::DataType;
use uuid;

use errors_chain::*;

pub struct SQLiteIndexStore {
    path: String,
    name: String,
    uncommitted_documents: usize,
    pub conn: Connection,
}

impl SQLiteIndexStore {
    /// Creates and returns a new SQLiteIndexStore. This store uses SQLite to
    /// store its data.
    pub fn new(name: &str, path: &str) -> Result<SQLiteIndexStore> {
        let final_path = path.to_string() + "/" + name + ".db";

        match Connection::open(final_path) {
            Ok(c) => {
                let store = SQLiteIndexStore {
                    name: name.to_string(),
                    path: path.to_string(),
                    uncommitted_documents: 0,
                    conn: c,
                };
                match store.initialize_tables() {
                    Ok(_) => Ok(store),
                    Err(e) => Err(ErrorKind::DatabaseError(e.to_string()).into()),
                }
            }
            Err(e) => Err(ErrorKind::DatabaseError(e.to_string()).into()),
        }
    }

    /// Wrapper function that calls all the table initialization functions. If any generate
    /// an error, that error is returned.
    fn initialize_tables(&self) -> Result<()> {
        match self.initialize_metadata_table() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_terms_table() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_fields_table() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_fields_index() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_documents_table() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_occurrences_table() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.set_metadata() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.enable_wal() {
            Ok(_) => {}
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_mappings_table() {
            Ok(_) => {},
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        match self.initialize_mappings_index() {
            Ok(_) => {},
            Err(e) => {
                return Err(ErrorKind::DatabaseError(e.to_string()).into());
            }
        };

        Ok(())
    }

    /// Initializes the metadata table in the SQLite database
    fn initialize_metadata_table(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_METADATA_TABLE, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_METADATA_TABLE.to_string())
            })
    }

    /// Initializes the terms table in the SQLite database
    fn initialize_terms_table(&self) -> Result<i32> {
        self.conn.execute(QUERY_CREATE_TERMS_TABLE, &[]).chain_err(
            || {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_TERMS_TABLE.to_string())
            },
        )
    }

    /// Initializes the terms table in the SQLite database
    fn initialize_fields_table(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_FIELDS_TABLE, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_FIELDS_TABLE.to_string())
            })

    }

    /// Initializes the fields index in the SQLite databse
    fn initialize_fields_index(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_FIELDS_INDEX, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_FIELDS_INDEX.to_string())
            })
    }

    /// Initializes the documents table in the SQLite database
    fn initialize_documents_table(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_DOCUMENTS_TABLE, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_DOCUMENTS_TABLE.to_string())
            })
    }

    /// Initializes the occurrences table in the SQLite database
    fn initialize_occurrences_table(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_OCCURRENCES_TABLE, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_OCCURRENCES_TABLE.to_string())
            })
    }

    /// Initializes the mappings table in the SQLite database
    fn initialize_mappings_table(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_MAPPINGS_TABLE, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_MAPPINGS_TABLE.to_string())
            })
    }

    fn initialize_mappings_index(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_CREATE_MAPPINGS_INDEX, &[])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_CREATE_MAPPINGS_INDEX.to_string())
            })
    }

    /// Sets the metadata on the table
    fn set_metadata(&self) -> Result<i32> {
        self.conn
            .execute(QUERY_INIT_METADATA, &[&self.name, &"false".to_string()])
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(QUERY_INIT_METADATA.to_string())
            })
    }

    fn enable_wal(&self) -> Result<()> {
        self.conn
            .execute_batch(PRAGMA_WAL)
            .chain_err(|| {
                ErrorKind::SQLQueryFailed(PRAGMA_WAL.to_string())
            })
    }
}

impl IndexStore for SQLiteIndexStore {
    fn exists(_name: &str) -> Result<bool> {
        Err(
            ErrorKind::NotYetImplemented("IndexExists".to_string()).into(),
        )
    }

    fn save(&self) -> Result<()> {
        Err(ErrorKind::NotYetImplemented("IndexSave".to_string()).into())
    }

    fn delete(&self) -> Result<()> {
        let final_path = self.path.to_string() + "/" + &self.name + ".db";
        match remove_file(final_path) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::IndexStoreDelete(self.name.to_string(), e.to_string()).into(),
                )
            }
        }
    }

    fn add_term(&self, term: &str) -> Result<()> {
        let mut stmt = self.conn.prepare_cached(QUERY_INSERT_TERM).unwrap();
        match stmt.execute(&[&term]) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::IndexAddTerm(self.name.to_string(), term.to_string(), e.to_string()).into(),
                )
            }
        }
    }

    fn add_field(&self, field: &str, document: &str, content: &str) -> Result<()> {
        let mut stmt = self.conn.prepare_cached(QUERY_INSERT_FIELD).unwrap();
        match stmt.execute(&[&field, &document, &content]) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::IndexAddField(field.to_string(), document.to_string(), e.to_string()).into(),
                )
            }
        }
    }

    /// Adds a mapping to the index database
    fn add_mapping(&self, field: &str, data_type: &DataType) -> Result<()> {
        let mut stmt = self.conn.prepare(QUERY_INSERT_MAPPING).unwrap();
        let dt = data_type.into_string().unwrap();
        match stmt.execute(&[&field, &dt]) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::IndexAddMapping(field.to_string(), dt, e.to_string()).into(),
                )
            }
        }
    }

    fn add_document(&mut self, document: &Document) -> Result<()> {
        if self.uncommitted_documents == 0 {
            let _ = self.conn.execute_batch("BEGIN TRANSACTION");
        }
        
        let mut stmt = self.conn.prepare(QUERY_INSERT_DOCUMENT).unwrap();
        match stmt.execute(&[&document.id_as_string(), &document.raw]) {
            Ok(_) => {
                self.uncommitted_documents += 1;
                if self.uncommitted_documents > 1000 {
                    let _ = self.conn.execute_batch("END TRANSACTION");
                    self.uncommitted_documents = 0;
                }
                
                Ok(())
            },
            Err(e) => {
                Err(
                    ErrorKind::IndexAddDocument(
                        document.id_as_string(),
                        self.name.to_string(),
                        e.to_string(),
                    ).into(),
                )
            }
        }
    }

    fn add_occurrence(&self, term: &str, occurrence: &Occurrence) -> Result<()> {
        let mut stmt = self.conn.prepare_cached(QUERY_INSERT_OCCURRENCE).unwrap();
        match stmt.execute(
            &[
                &term,
                &occurrence.document,
                &occurrence.field,
                &(occurrence.offset as i32),
            ],
        ) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::OccurrenceAdd(term.to_string(), self.name.to_string(), e.to_string()).into(),
                )
            }
        }
    }

    fn terms(&self) -> Result<Vec<String>> {
        let mut stmt = self.conn.prepare(QUERY_ALL_TERMS).unwrap();
        let mut results = Vec::<String>::new();
        let iter = stmt.query_map(&[], |row| row.get(0)).unwrap();

        for i in iter {
            results.push(i.unwrap());
        }
        Ok(results)
    }

    fn occurrences_for_term(&self, term: &str) -> Result<Vec<Occurrence>> {
        let mut stmt = self.conn.prepare(QUERY_OCCURRENCES_FOR_TERM).unwrap();
        let mut results = Vec::<Occurrence>::new();
        let iter = stmt.query_map(&[&term], |row| {
            Occurrence {
                document: row.get(0),
                field: row.get(1),
                offset: row.get(2),
            }
        }).unwrap();
        for i in iter {
            results.push(i.unwrap());
        }
        Ok(results)
    }

    fn count_terms(&self) -> Result<u32> {
        let mut stmt = self.conn.prepare(QUERY_COUNT_TERMS).unwrap();
        let result: u32 = stmt.query_row(&[], |r| r.get(0)).unwrap();
        Ok(result)
    }

    fn document_by_id(&self, document_id: &str) -> Result<Document> {
        let mut stmt = self.conn.prepare(QUERY_DOCUMENT_BY_ID).unwrap();
        let result: Document = stmt.query_row(&[&document_id.to_string()], |row| {
            let document_uuid: String = row.get(0);
            Document {
                id: uuid::Uuid::parse_str(&document_uuid).unwrap(),
                fields: HashMap::new(),
                raw: row.get(1),
            }
        }).unwrap();
        Ok(result)
    }

    fn documents_with_term_in_field(&self, field: &str, term: &str) -> Result<Vec<String>> {
        let mut results = vec![];
        let mut stmt = self.conn.prepare(QUERY_DOCUMENTS_WITH_TERM_IN_FIELD).unwrap();

        let iter = stmt.query_map(&[&field, &term], |row| {
            row.get(0)
        }).unwrap();

        for i in iter {
            results.push(i.unwrap());
        }
        Ok(results)
    }

    fn delete_document_by_id(&self, document_id: &str) -> Result<()> {
        let mut stmt = self.conn.prepare(QUERY_DELETE_DOCUMENT_BY_ID).unwrap();
        match stmt.execute(&[&document_id.to_string()]) {
            Ok(_c) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::DocumentDeleteError(document_id.to_string(), e.to_string()).into(),
                )
            }
        }
    }

    fn delete_occurrences_by_document_id(&self, document_id: &str) -> Result<()> {
        let mut stmt = self.conn
            .prepare(QUERY_DELETE_OCCURRENCES_BY_DOCUMENT_ID)
            .unwrap();
        match stmt.execute(&[&document_id.to_string()]) {
            Ok(_c) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::OccurrenceDelete(document_id.to_string(), e.to_string()).into(),
                )
            }
        }
    }
}