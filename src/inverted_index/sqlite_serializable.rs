use rusqlite::Connection;
use rusqlite::types::ToSql;

use errors_chain::*;
use cluster::node::NodeInfo;

/// This trait is implemented by things that want to be saved/loaded to our `SQLite` DB
pub trait SQLiteSerializable {
    fn create_table(c: &Connection) -> Result<()>;
    fn all(c: &Connection) -> Result<Vec<NodeInfo>>;
    fn find_by<T: ToSql>(key: String, value: T, c: &Connection) -> Result<Vec<NodeInfo>>;
    fn save(&self, c: &Connection) -> Result<()>;
    fn load(ip: String, port: String, c: Connection) -> Result<NodeInfo>;
    fn delete(&self, c: &Connection) -> Result<i32>;
}
