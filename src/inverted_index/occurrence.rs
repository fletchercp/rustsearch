/// This is one occurrence of a Term in a Document. It tracks the offset and document ID.
#[derive(Debug, Serialize, Deserialize)]
pub struct Occurrence {
    pub document: String,
    pub field: String,
    pub offset: u32,
}

impl Occurrence {
    /// Creates a returns a new Occurrence
    pub fn new(document_id: String, field: String, offset: u32) -> Occurrence {
        Occurrence {
            document: document_id,
            field: field,
            offset: offset,
        }
    }
}
