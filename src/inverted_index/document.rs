use std::collections::HashMap;

use serde_json;
use serde_json::{Value, Map};
use uuid;

/// A Document is a collection of fields, which may have one more more values
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Document {
    pub id: uuid::Uuid,
    pub fields: HashMap<String, String>,
    pub raw: String,
}

impl Document {
    /// Creates and returns a new Document
    pub fn new(s: &str) -> Document {
        let mut hash_map = HashMap::new();
        hash_map.insert("message".to_string(), s.to_string());
        Document {
            id: uuid::Uuid::new_v4(),
            fields: hash_map,
            raw: s.to_string(),
        }
    }

    /// Creates a Document from a hash of fields passed in
    pub fn from_fields(fields: HashMap<String, String>) -> Document {
        Document {
            id: uuid::Uuid::new_v4(),
            fields: fields,
            raw: "".to_string(),
        }
    }

    pub fn from_json(json: &str) -> Document {
        match serde_json::from_str(json) {
            Ok(ref parsed_json) => Document::new(json).fields(Document::flatten_fields(parsed_json)),
            Err(_) => {
                let mut fields: HashMap<String, String> = HashMap::new();
                fields.insert("message".to_string(), json.to_string());
                Document::new(json).fields(fields)
            }
        }
    }

    fn flatten_fields(map: &serde_json::Value) -> HashMap<String, String> {
        let depth = 0;
        let mut terms: HashMap<String, String> = HashMap::new();
        let mut fields: Vec<String> = vec![];
        fn f(map: &Map<String, Value>, depth: u32, fields: &mut Vec<String>, terms: &mut HashMap<String, String>) {
            for (k, v) in map {
                fields.push(k.to_string());
                if v.is_object() {
                    f(v.as_object().unwrap(), depth, fields, terms);
                    fields.pop();
                } else {
                    let field_key = fields.join(".");
                    fields.pop();
                    terms.insert(field_key, v.to_string());
                }
            }
        }
        f(map.as_object().unwrap(), depth, &mut fields, &mut terms);
        terms
    }

    pub fn fields(mut self, fields: HashMap<String, String>) -> Document {
        self.fields = fields;
        self
    }

    pub fn has_field(&self, field: &str) -> bool {
        self.fields.contains_key(field)
    }

    pub fn field_contains(&self, field: &str, term: &str) -> bool {
        match self.fields.get(field) {
            Some(v) => {
                v.contains(term)
            },
            None => {
                false
            }
        }
    }

    pub fn id_as_string(&self) -> String {
        self.id.hyphenated().to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test_create_document() {
        let d = Document::new("This is a test");
        assert_eq!(&d.raw, "This is a test");
        assert!(d.id_as_string().len() > 0);
    }
}
