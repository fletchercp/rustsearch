#[derive(Debug)]
pub struct TermResult {
    pub value: String,
    pub offset: usize,
}

/// Splits the value of a document on whitespace and returns a vector of terms and offsets
pub fn split_on_whitespace(value: &str) -> Vec<TermResult> {
    let mut result: Vec<TermResult> = Vec::new();
    for (i, term) in value.split_whitespace().enumerate() {
        let term_info = TermResult {
            value: term.to_string().to_lowercase(),
            offset: i,
        };
        result.push(term_info);
    }
    result
}
