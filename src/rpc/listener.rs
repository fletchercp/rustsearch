use std::net::{TcpListener, TcpStream};
use std::sync::{Arc, mpsc, RwLock};
use std::thread;
use std::io::{BufReader, BufRead};

/// This listens for new socket connections from other cluster members
pub fn listen(app_config: &Arc<RwLock<::AppConfig>>, tx: &mpsc::SyncSender<String>) {
    let bind_addr = app_config.read().unwrap().rpc_host.to_string() + ":" + &app_config.read().unwrap().rpc_port.to_string();
    match TcpListener::bind(bind_addr.clone()) {
        Ok(listener) => {
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        let clone_tx = tx.clone();
                        let _ = thread::spawn(move || { handle_client(stream, &clone_tx); });
                    }
                    Err(_) => {
                        println!("Connection failed!");
                    }
                }
            }
        },
        Err(e) => {
            println!("Error binding to RPC address {:?}. Error was: {:?}", bind_addr, e);
        }
    }
}

/// This handles a client connecting and reading data from it. It is used to accept commands and
/// send them over the channel to be processed.
fn handle_client(stream: TcpStream, tx: &mpsc::SyncSender<String>) {
    let mut reader = BufReader::new(stream);
    loop {
        let mut buf = String::new();
        match reader.read_line(&mut buf) {
            Err(e) => {
                println!("Error reading from client: {:?}", e);
                break;
            }
            Ok(m) => {
                if m == 0 {
                    // we've got an EOF
                    break;
                }
                match tx.send(buf.clone()) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("There was an error sending to a client: {:?}", e);
                    }
                }
            }
        };
    }
}
