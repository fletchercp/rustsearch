/// This contains various commands that can be serialized to JSON
use std::sync::{Arc, RwLock};
use serde_json;

use inverted_index::document::Document;
use cluster::node::NodeInfo;

/// Wraps a specific command to be sent to another Node.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CommandWrapper {
    // This is the ID of the node that generated the command
    pub id: String,
    // RPC hostname of the command sender
    pub rpc_host: String,
    // RPC port of the command sender
    pub rpc_port: i64,

    // Below are the commands that the wrapper can wrap
    pub create_index: Option<CreateIndex>,
    pub create_replica: Option<CreateReplica>,
    pub status_update: Option<StatusUpdate>,
    pub register_node: Option<RegisterNode>,
    pub get_nodes: Option<GetNodes>,
    pub index_document: Option<IndexDocument>,
    pub search: Option<Search>,
    pub search_results: Option<SearchResults>,
}

impl CommandWrapper {
    /// Creates and returns a new CommandWrapper
    pub fn new(app_config: &Arc<RwLock<::AppConfig>>) -> CommandWrapper {
        let ac = app_config.read().unwrap();
        CommandWrapper {
            id: ac.node_id.clone(),
            rpc_host: ac.rpc_host.clone(),
            rpc_port: ac.rpc_port,
            create_index: None,
            create_replica: None,
            status_update: None,
            register_node: None,
            get_nodes: None,
            index_document: None,
            search: None,
            search_results: None,
        }
    }

    /// Sets up the specific command that is inside the wrapper. To be used as part of the builder
    /// pattern for the wrapper.
    pub fn command(mut self, command: &str) -> CommandWrapper {
        match command {
            "create_index" => {
                self.create_index = Some(CreateIndex::new());
            }
            "create_replica" => {
                self.create_replica = Some(CreateReplica::new());
            }
            "status_update" => {
                self.status_update = Some(StatusUpdate::new());
            }
            "register_node" => {
                self.register_node = Some(RegisterNode::new());
            }
            "get_nodes" => {
                self.get_nodes = Some(GetNodes::new());
            }
            "index_document" => {
                self.index_document = Some(IndexDocument::new());
            }
            "search" => {
                self.search = Some(Search::new());
            }
            "search_results" => {
                self.search_results = Some(SearchResults::new());
            }
            _ => {}
        };
        self
    }

    /// Sets the index if wrapping an applicable command
    pub fn index(mut self, index: String) -> CommandWrapper {
        if self.create_index.is_some() {
            self.create_index.as_mut().unwrap().name = Some(index);
        } else if self.create_replica.is_some() {
            self.create_replica.as_mut().unwrap().name = Some(index);
        } else if self.index_document.is_some() {
            self.index_document.as_mut().unwrap().index = Some(index);
        } else if self.search.is_some() {
            self.search.as_mut().unwrap().index = Some(index);
        } else {

        }
        self
    }

    /// Sets the shard ID if wrapping an applicable command
    pub fn shard(mut self, shard: String) -> CommandWrapper {
        if self.create_index.is_some() {
            self.create_index.as_mut().unwrap().shard_id = Some(shard);
        } else if self.index_document.is_some() {
            self.index_document.as_mut().unwrap().shard_id = Some(shard);
        } else if self.search.is_some() {
            self.search.as_mut().unwrap().shard_id = Some(shard);
        } else {

        }
        self
    }

    /// Sets the replica ID if wrapping an applicable command
    pub fn replica(mut self, replica: String) -> CommandWrapper {
        if self.create_index.is_some() {
            self.create_index.as_mut().unwrap().replica_id = Some(replica);
        } else if self.create_replica.is_some() {
            self.create_replica.as_mut().unwrap().replica_id = Some(replica);
        } else if self.index_document.is_some() {
            self.index_document.as_mut().unwrap().replica_id = Some(replica);
        } else {

        }
        self
    }

    /// Sets the document for applicable commands
    pub fn document(mut self, document: Document) -> CommandWrapper {
        if self.index_document.is_some() {
            self.index_document.as_mut().unwrap().document = Some(document);
        } else {

        }
        self
    }

    /// Sets the request ID for applicable commands
    pub fn request_id(mut self, request_id: String) -> CommandWrapper {
        if self.search.is_some() {
            self.search.as_mut().unwrap().request_id = Some(request_id);
        } else if self.search_results.is_some() {
            self.search_results.as_mut().unwrap().request_id = Some(request_id);
        } else {

        }
        self
    }

    /// Sets the node for applicable commands
    #[allow(dead_code)]
    pub fn node(mut self, node: NodeInfo) -> CommandWrapper {
        if self.get_nodes.is_some() {
            if let Some(ref mut get_nodes) = self.get_nodes {   
                get_nodes.nodes.push(node);
            }
        } else {

        }
        self
    }

    /// Adds a vector of nodes to the nodes of a command
    #[allow(dead_code)]
    pub fn nodes(mut self, mut nodes: Vec<NodeInfo>) -> CommandWrapper {
        if self.get_nodes.is_some() {
            if let Some(ref mut get_nodes) = self.get_nodes {
                get_nodes.nodes.append(&mut nodes);   
            }
        } else {

        }
        self
    }

    /// Sets the query for applicable commands
    pub fn query(mut self, query: String) -> CommandWrapper {
        if let Some(ref mut s) = self.search {
            s.query = Some(query);
        }
        self
    }

    /// Converts the command wrapper to a JSON string
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

/// This is a command to create an Index
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CreateIndex {
    // Name of the index to create
    pub name: Option<String>,
    // ID of the index shard
    pub shard_id: Option<String>,
    // ID of the replica shard if we are indexing to a replica
    pub replica_id: Option<String>,
}

impl CreateIndex {
    /// Creates and returns a new CreateIndex command
    pub fn new() -> CreateIndex {
        CreateIndex {
            name: None,
            shard_id: None,
            replica_id: None,
        }
    }
}

/// This is a command to create an Replica
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CreateReplica {
    // ID of the index shard
    pub replica_id: Option<String>,
    pub name: Option<String>,
}

impl CreateReplica {
    /// Creates and returns a new CreateReplica command
    pub fn new() -> CreateReplica {
        CreateReplica {
            name: None,
            replica_id: None,
        }
    }
}

/// This is a command to update the master with our current status
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StatusUpdate {
    // Current CPU usage of the Node
    cpu: Option<f32>,
    // Current % of RAM free on the Node
    free_memory: Option<f32>,
    // Current % of disk free
    disk_free: Option<f32>,
}

impl StatusUpdate {
    /// Creates and returns a new CreateIndex command
    pub fn new() -> StatusUpdate {
        StatusUpdate {
            cpu: None,
            free_memory: None,
            disk_free: None,
        }
    }
}

/// This is a command to register a node. It only needs the RPC host and port, which is
/// included in the command wrapper, so it has no attributes.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RegisterNode {}

impl RegisterNode {
    /// Creates and returns a new RegisterNode command
    pub fn new() -> RegisterNode {
        RegisterNode {}
    }
}

/// Command to get all the Nodes in the cluster
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GetNodes {
    /// Vector of Nodes to return as a result. This is empty if a Node is requesting the other Nodes.
    nodes: Vec<NodeInfo>,
}

impl GetNodes {
    /// Creates and returns a GetNodes
    pub fn new() -> GetNodes {
        GetNodes { nodes: vec![] }
    }
}

/// This is a command to Index a document to a specific shard
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct IndexDocument {
    pub document: Option<Document>,
    pub index: Option<String>,
    pub shard_id: Option<String>,
    pub replica_id: Option<String>,
}

impl IndexDocument {
    /// Creates and returns a new IndexDocument command
    pub fn new() -> IndexDocument {
        IndexDocument {
            document: None,
            index: None,
            shard_id: None,
            replica_id: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Search {
    pub request_id: Option<String>,
    pub index: Option<String>,
    pub query: Option<String>,
    pub shard_id: Option<String>,
}

impl Search {
    pub fn new() -> Search {
        Search {
            request_id: None,
            index: None,
            query: None,
            shard_id: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SearchResult {
    pub document_id: String,
    pub offsets: Vec<u32>,
    pub took: Option<f64>,
}

impl SearchResult {
    /// Creates and returns a new SearchResult
    pub fn new(document_id: String) -> SearchResult {
        SearchResult {
            document_id: document_id,
            offsets: Vec::new(),
            took: None,
        }
    }

    /// Adds an offset (location in a document field) to the result
    pub fn add_offset(&mut self, offset: u32) {
        self.offsets.push(offset);
    }
}

/// Contains all the `SearchResult` structs that are created in response to a search
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SearchResults {
    pub request_id: Option<String>,
    pub results: Vec<SearchResult>,
}

impl SearchResults {
    /// Creates and returns a new SearchResults
    pub fn new() -> SearchResults {
        SearchResults {
            request_id: None,
            results: vec![],
        }
    }

    /// Adds an occurrence to the search results
    pub fn add_occurrence(&mut self, document_id: String, offset: u32) {
        &mut for ref mut result in &mut self.results {
            if result.document_id == document_id {
                result.add_offset(offset);
                return;
            }
        };
        let mut new_result = SearchResult::new(document_id);
        new_result.add_offset(offset);
        self.results.push(new_result);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    fn generate_app_config() -> ::AppConfig {
        ::AppConfig {
            node_id: "test_id".to_string(),
            data_directory: "/tmp/rusticsearch_test".to_string(),
            master_host: "127.0.0.1".to_string(),
            master_port: 9000,
            rpc_host: "127.0.0.1".to_string(),
            rpc_port: 9001,
            is_master: true,
            is_data: true
        }
    }

    #[test]
    fn test_command_wrapper() {
        let app_config = Arc::new(RwLock::new(generate_app_config()));
        let command = CommandWrapper::new(&app_config.clone());
        assert_eq!(command.id, app_config.read().unwrap().node_id);
    }

    #[test]
    fn test_create_index() {
        let command = CreateIndex::new();
        assert_eq!(command.name, None);
        assert_eq!(command.shard_id, None);
    }

    #[test]
    fn test_status_update() {
        let command = StatusUpdate::new();
        assert_eq!(command.cpu, None);
        assert_eq!(command.free_memory, None);
        assert_eq!(command.disk_free, None);
    }

    #[test]
    fn test_register_node() {
        let _command = RegisterNode::new();
    }

    #[test]
    fn test_get_nodes() {
        let command = GetNodes::new();
        assert_eq!(command.nodes.len(), 0);
    }

    #[test]
    fn test_index_document() {
        let command = IndexDocument::new();

        assert_eq!(command.index, None);
        assert_eq!(command.shard_id, None);
    }

    #[test]
    fn test_search() {
        let command = Search::new();
        assert_eq!(command.request_id, None);
        assert_eq!(command.index, None);
        assert_eq!(command.query, None);
        assert_eq!(command.shard_id, None);
    }

    #[test]
    fn test_search_result() {
        let command = SearchResult::new("1234".to_string());
        assert_eq!(command.document_id, "1234".to_string());
        assert_eq!(command.offsets.len(), 0);
    }

    #[test]
    fn test_search_results() {
        let command = SearchResults::new();
        assert_eq!(command.request_id, None);
        assert_eq!(command.results.len(), 0);
    }
}
