/// Handles processing RPCs from other cluster members

use std::sync::{Arc, RwLock, mpsc};
use std::thread;
use std::time::SystemTime;
use serde_json;

use rpc::commands;
use rpc::commands::CommandWrapper;
use database::db::DBManager;
use inverted_index::index_sqlite::SQLiteIndex;
use inverted_index::index_store::Index;
use cluster::mailbox;
use cluster::node::NodeInfo;
use cluster::node_manager::NodeManager;
use inverted_index::sqlite_serializable::SQLiteSerializable;
use cluster::trackers::{StateTracker, NodeState};
use thread_pool;
use search;

/// Handles an incoming command from a connected client
pub fn handle_command(
    rx: &mpsc::Receiver<String>,
    state_tracker: &Arc<RwLock<StateTracker>>,
    node_manager: &Arc<RwLock<NodeManager>>,
    app_config: &Arc<RwLock<::AppConfig>>,
    mailbox: &Arc<RwLock<mailbox::Mailbox>>,
    mut tp: thread_pool::ThreadPool,
) {

    let mut dbm = DBManager::new(Arc::clone(app_config)).unwrap();
    let cluster_db = dbm.cluster_db().expect(
        "Unable to access cluster database in handle_command",
    );

    loop {
        let msg = rx.recv();
        let cmd = parse_command(&msg.unwrap());
        if cmd.create_index.is_some() {
            let mut tmp = cmd.create_index.unwrap();
            let name = tmp.name.as_mut().unwrap().to_string() + "-" + &tmp.shard_id.unwrap();
            let _idx = SQLiteIndex::new(&name, &app_config.read().unwrap().data_directory);
        } else if cmd.create_replica.is_some() {
            let mut tmp = cmd.create_replica.unwrap();
            let name = tmp.name.as_mut().unwrap().to_string() + "-" + &tmp.replica_id.unwrap() + "-replica";
            let _replica = SQLiteIndex::new(&name, &app_config.read().unwrap().data_directory);
        } else if cmd.status_update.is_some() {
            println!("Node wants to update status");
        } else if cmd.register_node.is_some() {
            if app_config.read().unwrap().is_master {
                let mut sender_node = extract_sender_data(&cmd);
                sender_node.connect();
                if sender_node.save(cluster_db).is_err() {
                    continue;
                }
                let response = commands::CommandWrapper::new(&Arc::clone(app_config)).command("register_node");
                sender_node.send_command(&response);
            } else {
                let mut locked = state_tracker.write().unwrap();
                locked.switch(NodeState::Registered);
                continue;
            }
        } else if cmd.get_nodes.is_some() {
            if app_config.read().unwrap().is_master {
                let mut locked = node_manager.write().unwrap();
                let response = commands::CommandWrapper::new(&Arc::clone(app_config))
                    .command("get_nodes")
                    .nodes(locked.nodes());
                match locked.broadcast_command(&response) {
                    Ok(_) => {
                        continue;
                    }
                    Err(e) => {
                        println!("Error broadcasting command: {:?}", e.to_string());
                        continue;
                    }
                }
            } else {
                println!(
                    "Received list of nodes from master: {:?}",
                    cmd.get_nodes.unwrap()
                );
                continue;
            }
        } else if cmd.index_document.is_some() {
            let _now = SystemTime::now();
            // Here we've received a command to index a document. That document should be contained in
            // the JSON with the request.
            let tmp = cmd.index_document.unwrap();
            let index_name: String;
            if tmp.shard_id.is_some() {
                index_name = tmp.index.as_ref().unwrap().to_string() + "-" + &tmp.shard_id.unwrap();
            } else if tmp.replica_id.is_some() {
                index_name = tmp.index.as_ref().unwrap().to_string() + "-" + &tmp.replica_id.unwrap() + "-replica";
            } else {
                continue;
            }

            match tmp.document {
                Some(doc) => {
                    let j = thread_pool::IndexJob::new(
                        doc.clone(),
                        index_name.to_string(),
                        app_config.read().unwrap().data_directory.to_string(),
                    );
                    tp.execute(j);
                }
                None => {
                    continue;
                }
            };
            //println!("Indexing took: {:?}", now.elapsed().unwrap());
            continue;
        } else if cmd.search.is_some() {
            let app_config_clone = Arc::clone(app_config);
            let node_manager_clone = Arc::clone(node_manager);
            thread::spawn(move || {
                let query_json = cmd.clone().search.unwrap().query.unwrap();
                let q = search::query::Q::from_json(&query_json).unwrap();
                let request_id = cmd.search
                  .as_ref()
                  .unwrap()
                  .request_id
                  .as_ref()
                  .unwrap()
                  .to_string();

                let shard = cmd.search.as_ref().unwrap().shard_id.clone();
                let index_name = cmd.search
                  .as_ref()
                  .unwrap()
                  .index
                  .as_ref()
                  .unwrap()
                  .to_string() + "-" + shard.as_ref().unwrap();

                match SQLiteIndex::new(
                  &index_name,
                  &app_config_clone.read().unwrap().data_directory,
                ) {
                  Ok(idx) => {
                      let _results = idx.process_query(q);
                      let response = CommandWrapper::new(&Arc::clone(&app_config_clone))
                          .command("search_results")
                          .request_id(request_id);

                      let mut locked = node_manager_clone.write().unwrap();

                      let json = serde_json::to_string(&response).unwrap();
                      match locked.send_to_node(&cmd.id, json) {
                          Ok(_) => {}
                          Err(e) => {
                              println!("Error sending command to node: {:?}", e);
                          }
                      }
                  }
                  Err(e) => {
                      println!("Error opening index for search request: {:?}", e);
                  }
              };
          });
        } else if cmd.search_results.is_some() {
            let mut locked = mailbox.write().unwrap();
            match locked.deliver_search_results(cmd.search_results.unwrap()) {
                Ok(_) => {
                    continue;
                }
                Err(e) => {
                    println!("Error delivering search results: {:?}", e);
                }
            };
        } else {
            println!("Unknown command received: {:?}", cmd);
            continue;
        }
    }
}

/// Parses a command out of a message
fn parse_command(cmd: &str) -> commands::CommandWrapper {
    // TODO: Need to test this before unwrapping?
    serde_json::from_str(cmd).unwrap()
}

fn extract_sender_data(cmd: &commands::CommandWrapper) -> NodeInfo {
    NodeInfo::new(cmd.id.clone(), cmd.rpc_host.clone(), cmd.rpc_port)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rpc::commands;
    use rpc::handlers;
    #[test]
    fn test_parse_command() {
        let test_cw = commands::CommandWrapper {
            id: "test_id".to_string(),
            rpc_host: "127.0.0.1".to_string(),
            rpc_port: 9000,
            create_index: None,
            create_replica: None,
            status_update: None,
            register_node: None,
            get_nodes: None,
            index_document: None,
            search: None,
            search_results: None,
        };
        let json = serde_json::to_string(&test_cw);
        assert_eq!(json.is_ok(), true);
        let parsed = handlers::parse_command(&json.unwrap());
        assert_eq!(parsed.id, "test_id".to_string());
    }
}
