use std::net::TcpStream;
use std::fmt;
use std::io::Write;

use serde_json;
use sys_info::LoadAvg;

use inverted_index::sqlite_serializable::SQLiteSerializable;
use rusqlite::Connection;
use rusqlite::types::ToSql;
use errors_chain::*;
use database::queries::*;
use rpc::commands::CommandWrapper;

/// Contains information about nodes in the cluster and maintains a socket connection to them
#[derive(Debug, Serialize, Default, Deserialize)]
pub struct NodeInfo {
    pub id: String,
    pub mgmt_host: String,
    pub mgmt_port: i64,
    pub is_master: bool,
    pub is_data: bool,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    pub socket: Option<TcpStream>,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    cpu_load: Option<LoadAvg>,
}


impl NodeInfo {
    pub fn load_my_id(c: &Connection) -> Result<String> {
        let mut stmt = c.prepare(QUERY_GET_CONFIG_ID).chain_err(
            || QUERY_GET_CONFIG_ID,
        )?;
        match stmt.query_row(&[], |row| row.get(0)) {
            Ok(id) => Ok(id),
            Err(e) => Err(ErrorKind::SQLQueryFailed(e.to_string()).into()),
        }
    }

    /// Saves the ID of the node to the database
    pub fn save_my_id(&self, c: &Connection) -> Result<()> {
        let mut stmt = c.prepare(QUERY_INSERT_CONFIG_ID).chain_err(
            || QUERY_INSERT_CONFIG_ID,
        )?;
        stmt.execute(&[&self.id]).chain_err(|| "Error saving ID")?;
        Ok(())
    }

    /// Creates and returns a new NodeInfo, which is used to track state about our cluster.
    /// Also contains a communications socket so we can talk to the cluster
    pub fn new(id: String, mgmt_host: String, mgmt_port: i64) -> NodeInfo {
        NodeInfo {
            id: id,
            mgmt_host: mgmt_host,
            mgmt_port: mgmt_port,
            is_master: false,
            is_data: true,
            socket: None,
            cpu_load: None,
        }
    }

    /// Attempts to establish a TCP connection to the node
    pub fn connect(&mut self) {
        match TcpStream::connect(self.mgmt_host.clone() + ":" + &self.mgmt_port.to_string()) {
            Ok(conn) => {
                self.socket = Some(conn);
            }
            Err(e) => {
                println!(
                    "Unable to connect to node {:?} [{:?}:{:?}]: {:?}",
                    self.id,
                    self.mgmt_host,
                    self.mgmt_port,
                    e
                );
            }
        };
    }

    /// Sends a string to the cluster. Primarily used to send RPC commands.
    pub fn send(&mut self, msg: String) -> Result<()> {
        // No need to continue if the socket isn't connected
        if self.socket.is_none() {
            return Err(ErrorKind::NodeSocketFailed(self.id.clone()).into());
        }
        // The listener socket reads until \n is found, so we need to add one
        // TODO: Should this be added via the to_json methods?
        let msg = msg + "\n";
        // TODO: Do we still need to call flush() here?
        match self.socket.as_ref().unwrap().write(msg.as_bytes()) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    ErrorKind::NodeSendFailed(self.id.clone(), e.to_string()).into(),
                )
            }
        }
    }

    /// Sends a string to the cluster. Primarily used to send RPC commands.
    pub fn send_command(&mut self, cmd: &CommandWrapper) {
        let msg = serde_json::to_string(cmd).unwrap();
        let msg = msg + "\n";
        // TODO: Do we still need to call flush() here?
        match self.socket {
            Some(ref mut socket) => {
                match socket.write(msg.as_bytes()) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error writing data to socket: {:?}", e);
                    }
                }
            }
            None => {
                println!("Attempt to send to a null socket!");
            }
        }
    }
}

impl fmt::Display for NodeInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Host: {}, Port: {}", self.mgmt_host, self.mgmt_port)
    }
}

impl Clone for NodeInfo {
    fn clone(&self) -> NodeInfo {
        NodeInfo {
            id: self.id.clone(),
            mgmt_host: self.mgmt_host.clone(),
            mgmt_port: self.mgmt_port,
            is_master: self.is_master,
            is_data: self.is_data,
            socket: None,
            cpu_load: None,
        }
    }
}

impl SQLiteSerializable for NodeInfo {
    /// Static method that creates the nodes table if needed
    fn create_table(c: &Connection) -> Result<()> {
        match c.execute(QUERY_CREATE_NODES_TABLE, &[]) {
            Ok(_) => Ok(()),
            Err(_) => {
                Err(
                    ErrorKind::SQLQueryFailed(QUERY_CREATE_NODES_TABLE.to_string()).into(),
                )
            }
        }
    }

    /// Static method that returns all of the NodeInfos in the database
    fn all(c: &Connection) -> Result<Vec<NodeInfo>> {
        let mut result = Vec::<NodeInfo>::new();
        let stmt = c.prepare(QUERY_ALL_NODES);
        match stmt {
            Ok(mut s) => {
                let node_iter = s.query_map(&[], |row| NodeInfo::new(row.get(0), row.get(1), row.get(2)))
                    .unwrap();
                for node in node_iter {
                    result.push(node.unwrap());
                }
                Ok(result)

            }
            Err(_) => {
                Err(
                    ErrorKind::SQLQueryFailed(QUERY_ALL_NODES.to_string()).into(),
                )
            }
        }
    }

    /// Static method that searches by one generic key
    fn find_by<T: ToSql>(key: String, value: T, c: &Connection) -> Result<Vec<NodeInfo>> {
        let mut result = Vec::<NodeInfo>::new();
        // The replace call is here because we want to be able to have both dynamic keys and values
        // when using find_by. Unfortunately, in rusqlite we cannot use :<whatever> for a column
        // name, so we need to do a simple search and replace before we prepare the query for the
        // database
        let q = QUERY_FIND_NODE_BY.replace("{}", &key);
        let mut stmt = c.prepare(&q).chain_err(|| QUERY_FIND_NODE_BY)?;
        let node_iter = stmt.query_map_named(&[(":value", &value)], |row| {
            NodeInfo::new(row.get(0), row.get(1), row.get(2))
        }).chain_err(|| QUERY_FIND_NODE_BY)?;
        for node in node_iter {
            match node {
                Ok(n) => {
                    result.push(n);
                }
                Err(e) => {
                    println!("Error adding NodeInfo to results in find_by: {:?}", e);
                }
            };
        }
        Ok(result)
    }

    /// Saves a NodeInfo to the database
    fn save(&self, c: &Connection) -> Result<()> {
        let mut stmt = c.prepare(QUERY_INSERT_NODE).chain_err(|| QUERY_INSERT_NODE)?;
        stmt.execute(
            &[
                &self.id,
                &self.mgmt_host,
                &self.mgmt_port,
                &self.is_master,
                &self.is_data,
            ],
        ).chain_err(|| QUERY_INSERT_NODE)?;
        Ok(())
    }

    /// Loads a NodeInfo based on its IP and Port values
    fn load(ip: String, port: String, c: Connection) -> Result<NodeInfo> {
        let mut stmt = c.prepare(QUERY_FIND_NODE_BY_IP_PORT).unwrap();
        Ok(
            stmt.query_row(&[&ip, &port], |row| {
                NodeInfo::new(row.get(0), row.get(1), row.get(2))
            }).unwrap(),
        )
    }

    /// Deletes a NodeInfo from the database
    fn delete(&self, c: &Connection) -> Result<i32> {
        let mut stmt = c.prepare(QUERY_DELETE_NODE_BY_IP_PORT).unwrap();
        match stmt.execute(&[&self.mgmt_host, &self.mgmt_port.to_string()]) {
            Ok(r) => Ok(r),
            Err(_) => {
                Err(
                    ErrorKind::SQLQueryFailed(QUERY_DELETE_NODE_BY_IP_PORT.to_string()).into(),
                )
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_node() {
        let mut _node = NodeInfo::new("test-node".to_string(), "localhost".to_string(), 20000);
    }
}
