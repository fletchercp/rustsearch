/// Various states the Node can be in
pub enum NodeState {
    NotRegistered,
    Registering,
    Registered,
}

/// Struct that let's us track and change our state
pub struct StateTracker {
    pub current_state: NodeState,
}

impl StateTracker {
    pub fn new() -> StateTracker {
        StateTracker { current_state: NodeState::NotRegistered }
    }

    pub fn switch(&mut self, new_state: NodeState) {
        self.current_state = new_state;
    }
}
