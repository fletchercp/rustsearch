/// NodeManager keeps track of Nodes in the cluster from the perspective
/// of Node it is on

use std::sync::{Arc, RwLock, mpsc};

use errors_chain::*;
use rand;
use rand::Rng;
use rusqlite::Connection;
use serde_json;

use cluster::node::NodeInfo;
use constants;
use database::queries::*;
use inverted_index::sqlite_serializable::SQLiteSerializable;
use rpc::commands;
use rpc::commands::CommandWrapper;




/// Maintains a list of all nodes in the cluster and monitors their connections
pub struct NodeManager {
    /// ID of the current Node
    pub node_id: String,
    /// This is the address the local Node is listening on to receive RPC commands
    pub rpc_host: Option<String>,
    /// Port the local Node is listening on to receive RPC commands
    pub rpc_port: Option<i64>,
    /// Which Node the local Node thinks is the master
    pub master: Option<NodeInfo>,
    /// Vector of all other known Nodes in the cluster
    pub nodes: Vec<NodeInfo>,
    /// Shared configuration struct
    pub app_config: Arc<RwLock<::AppConfig>>,
    /// Channel used to send commands to other Nodes
    pub tx: Option<mpsc::SyncSender<String>>,
    /// Path to database containing the cluster metadata for this Node
    cluster_db: String,
}

    /// Returns a Mailbox
    ///
    /// # Arguments
    /// * This function takes no arguments
    ///
    /// # Example
    ///
    /// ```
    /// use cluster::Mailbox;
    /// let mailbox = Mailbox::new();
    /// ```
    /// 
impl NodeManager {
    /// Creates and returns a new NodeManager
    /// 
    /// # Arguments
    /// * `app_config` - Configuration struct that the NodeManager uses to configure various things
    /// 
    /// # Example
    /// 
    /// ```
    /// use cluster::node_manager;
    /// use std::sync::{Arc, RwLock, mpsc};
    /// use ::AppConfig;
    /// let ac = AppConfig::new_from_defaults();
    /// let nm = NodeManager::new(Arc::new(RwLock::new(ac)));
    /// ```
    /// 
    /// # Errors
    /// 
    /// Errors are returned if the NodeManager is unable to properly access the cluster database
    
    pub fn new(app_config: &Arc<RwLock<::AppConfig>>) -> Result<NodeManager> {
        let db_path = app_config.read().unwrap().data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME;
        let mut new_manager = NodeManager {
            node_id: app_config.read().unwrap().node_id.clone(),
            nodes: vec![],
            rpc_host: None,
            rpc_port: None,
            app_config: Arc::clone(app_config),
            cluster_db: db_path,
            master: None,
            tx: None,
        };

        if !new_manager.app_config.read().unwrap().is_master {
            let mut master = NodeInfo::new(
                "UNKNOWN".to_string(),
                app_config.read().unwrap().master_host.to_string(),
                app_config.read().unwrap().master_port,
            );
            master.connect();
            new_manager.master = Some(master);
        }

        let conn = Connection::open(new_manager.cluster_db.clone()).unwrap();
        conn.execute(QUERY_CREATE_NODES_TABLE, &[]).chain_err(
            || "Unable to create nodes table",
        )?;
        conn.execute(QUERY_CREATE_INDEXES_TABLE, &[]).chain_err(
            || "Unable to create indices table",
        )?;
        conn.execute(QUERY_CREATE_SHARDS_TABLE, &[]).chain_err(
            || "Unable to create shards table",
        )?;
        conn.execute(QUERY_CREATE_REPLICAS_TABLE, &[]).chain_err(
            || "Unable to create replicas table",
        )?;
        Ok(new_manager)
    }

    /// Adds a transmit channel to the NodeManager
    pub fn tx(&mut self, tx: mpsc::SyncSender<String>) {
        self.tx = Some(tx);
    }

    /// Selects a random node from the Nodes we know about
    pub fn random_data_node(&mut self) -> Option<&mut NodeInfo> {
        rand::thread_rng().choose_mut(&mut self.nodes)
    }

    /// Adds a node to the NodeManager
    pub fn add_node(&mut self, mut n: NodeInfo) -> Result<()> {
        n.connect();
        let conn = Connection::open(self.cluster_db.clone()).chain_err(
            || "Unable to open connection to cluster db to add node",
        )?;
        n.save(&conn).chain_err(
            || "Unable to save new node to database",
        )?;
        self.nodes.insert(0, n);
        Ok(())
    }

    /// Sends a string to a specific node
    pub fn send_to_node(&mut self, id: &str, msg: String) -> Result<()> {
        // First check to see if we are sending to ourself. If so, take the shortcut and send it
        // right to the channel, no need to go over a socket.
        if id == self.node_id {
            match self.tx {
                Some(ref mut tx) => {
                    tx.send(msg).chain_err(|| "Unable to send message to node")?;
                    return Ok(());
                }
                None => {
                    return Ok(());
                }
            };
        }

        for node in &mut self.nodes {
            if node.id == id {
                match node.send(msg) {
                    Ok(_) => {
                        return Ok(());
                    }
                    Err(e) => {
                        return Err(format!("Error sending to node {:?}", e).into());
                    }
                }
            }
        }
        Err("Node not found when trying to send message".into())
    }

    /// Inserts a record assigning a shard to a specific node
    pub fn assign_shard_to_node(&mut self, index: &str, shard_id: &str, node_address: &str, node_port: i64) -> Result<()> {
        for node in &self.nodes {
            if node.mgmt_host == node_address && node.mgmt_port == node_port {
                let node_id = self.id(node_address, node_port)?;
                let conn = Connection::open(self.cluster_db.clone()).chain_err(
                    || "Unable to connect to cluster database",
                )?;
                let mut stmt = conn.prepare(QUERY_INSERT_SHARD).chain_err(
                    || "Unable to prepare query INSERT_SHARD",
                )?;
                match stmt.execute(&[&shard_id, &index, &node_id]) {
                    Ok(_) => {
                        return Ok(());
                    }
                    Err(e) => {
                        return Err(format!("Error inserting into shard table: {:?}", e).into());
                    }
                };
            }
        }
        Ok(())
    }

    /// Removes all the nodes from the cluster metadata database
    pub fn clear_nodes(&self) -> Result<()> {
        // TODO: Check for error here. Need a better return type.
        let conn = Connection::open(self.cluster_db.clone()).unwrap();
        match conn.execute(QUERY_DELETE_ALL_NODES, &[]) {
            Ok(_) => Ok(()),
            Err(e) => {
                Err(
                    format!("Unable to clear nodes in node table: {:?}", e).into(),
                )
            }
        }
    }

    /// Returns the ID of a node
    pub fn id(&self, node_address: &str, node_port: i64) -> Result<i32> {
        let conn = Connection::open(self.cluster_db.clone()).chain_err(
            || "Unable to open cluster db",
        )?;
        let mut stmt = conn.prepare(QUERY_GET_NODE_ID).chain_err(
            || "Unable to prepare GET_NODE_ID query",
        )?;
        stmt.query_row(&[&node_address, &node_port], |row| Ok(row.get(0)))
            .chain_err(|| "Querying database for node ID failed")?
    }

    /// Sends a command to all nodes
    pub fn broadcast_command(&mut self, cmd: &CommandWrapper) -> Result<()> {
        let json = serde_json::to_string(cmd).unwrap();
        for node in &mut self.nodes {
            if node.id != cmd.id {
                match node.send(json.clone()) {
                    Ok(_) => return Ok(()),
                    Err(e) => {
                        return Err(format!("Error broadcasting command: {:?}", e).into());
                    }
                }
            }
        }
        Ok(())
    }

    /// Returns all the nodes as a JSON string
    pub fn nodes_as_json(&self) -> String {
        let mut results: Vec<String> = vec![];
        for node in &self.nodes {
            let j = serde_json::to_string(node).unwrap();
            results.push(j);
        }
        serde_json::to_string(&results).unwrap()
    }

    /// Returns a cloned set of the nodes
    pub fn nodes(&self) -> Vec<NodeInfo> {
        self.nodes.clone()
    }

    /// Gets a current list of nodes from the master
    pub fn refresh_nodes(&mut self) -> Result<()> {
        match self.master {
            Some(ref mut m) => {
                match m.send(
                    commands::CommandWrapper::new(&Arc::clone(&self.app_config))
                        .command("get_nodes")
                        .to_json(),
                ) {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("Error refreshing nodes: {:?}", e).into()),
                }
            }
            None => Err("No master configured".into()),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    fn generate_app_config() -> ::AppConfig {
        ::AppConfig {
            node_id: "test_id".to_string(),
            data_directory: "/tmp/rusticsearch_test".to_string(),
            master_host: "127.0.0.1".to_string(),
            master_port: 9000,
            rpc_host: "127.0.0.1".to_string(),
            rpc_port: 9001,
            is_master: true,
            is_data: true,
        }
    }

    #[test]
    fn test_node_manager() {
        let ac = Arc::new(RwLock::new(generate_app_config()));
        let nm = NodeManager::new(&ac);
        assert!(nm.is_ok(), true);
        let mut nm = nm.unwrap();
        let node = NodeInfo::new("test-node".to_string(), "localhost".to_string(), 20000);
        let result = nm.add_node(node);
        assert!(result.is_ok(), true);
    }
}
