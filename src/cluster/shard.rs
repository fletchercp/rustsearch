use rusqlite::Connection;
use database::queries::*;
use serde_json;
use errors_chain::*;

/// Shard is a piece of an index that can be read from or written to
#[derive(Debug, Serialize, Deserialize)]
pub struct Shard {
    pub id: String,
    pub node: String,
    pub index: String,
}

impl Shard {
    /// Saves a Shard to the database
    pub fn save(&self, c: &Connection) -> Result<()> {
        let mut stmt = c.prepare(QUERY_INSERT_SHARD).chain_err(
            || QUERY_INSERT_SHARD,
        )?;
        stmt.execute(&[&self.id, &self.index, &self.node])
            .chain_err(|| QUERY_INSERT_SHARD)?;
        Ok(())
    }

    /// Sets the ID of the Shard
    pub fn id(mut self, id: String) -> Shard {
        self.id = id;
        self
    }

    /// Sets the NodeID of a Shard, that is, what Node it lives on
    pub fn node(mut self, node: String) -> Shard {
        self.node = node;
        self
    }

    /// Sets the Index of a Shard
    pub fn index(mut self, index: String) -> Shard {
        self.index = index;
        self
    }

    /// Returns the JSON representation of a Shard
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

/// Replica is a copy of a shard that is read-only. It is used to maintain multiple copies of data.
#[derive(Debug, Serialize, Deserialize)]
pub struct Replica {
    pub id: String,
    pub node: String,
    pub index: String,
    pub shard: String,
}

impl Replica {
    /// Saves a Replica to the database
    pub fn save(&self, c: &Connection) -> Result<()> {
        let mut stmt = c.prepare(QUERY_INSERT_REPLICA).chain_err(|| {
            QUERY_INSERT_REPLICA.to_string()
        })?;
        stmt.execute(&[&self.id, &self.index, &self.node, &self.shard])
            .chain_err(|| QUERY_INSERT_REPLICA.to_string())?;
        Ok(())
    }

    /// Sets the ID of the Replica
    pub fn id(mut self, id: String) -> Replica {
        self.id = id;
        self
    }

    /// Sets the NodeID of a Replica, that is, what Node it lives on
    pub fn node(mut self, node: String) -> Replica {
        self.node = node;
        self
    }

    /// Sets the Index of a Replica
    pub fn index(mut self, index: String) -> Replica {
        self.index = index;
        self
    }

    /// Sets the Shard of the Replica
    pub fn shard(mut self, shard: String) -> Replica {
        self.shard = shard;
        self
    }
    /// Returns the JSON representation of a Shard
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}
