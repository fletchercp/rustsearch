/// Mailbox is a simple wrapper around a `HashMap` that can be used for storing
/// and retrieving search results

use std::collections::HashMap;
use errors_chain::*;
use rpc::commands::SearchResults;

/// Datastructure that stores search results or other messages for things to later pick up
#[derive(Debug)]
pub struct Mailbox {
    /// HashMap that stores search results. Worker threads deliver their results to this for
    /// later pickup by the caller
    search_results: HashMap<String, Vec<SearchResults>>,
}

impl Mailbox {
    /// Returns a Mailbox
    ///
    /// # Arguments
    /// * This function takes no arguments
    ///
    /// # Example
    ///
    /// ```
    /// use cluster::Mailbox;
    /// let mailbox = Mailbox::new();
    /// ```
    pub fn new() -> Mailbox {
        Mailbox { search_results: HashMap::new() }
    }

    /// Delivers search results to a Node's Mailbox
    ///
    /// # Arguments
    /// * `search_results` - A struct that contains all the search results
    pub fn deliver_search_results(&mut self, search_results: SearchResults) -> Result<()> {
        if search_results.request_id.is_none() {
            return Err("Search results had no request ID".into());
        }

        let request_id = search_results.request_id.as_ref().unwrap().clone();
        if !self.search_results.contains_key(&request_id) {
            self.search_results.insert(request_id.clone(), vec![]);
        }

        self.search_results.get_mut(&request_id).unwrap().push(
            search_results,
        );
        Ok(())
    }

    /// Retrieves the search results from a Node's Mailbox
    ///
    /// # Arguments
    /// * `request_id` - The UUID in string form of the request we want results for
    pub fn get_search_results(&mut self, request_id: &str) -> Option<Vec<SearchResults>> {
        self.search_results.remove(request_id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rpc::commands::SearchResults;
    #[test]
    fn create_mailbox() {
        let mut mb = Mailbox::new();
        assert_eq!(mb.search_results.len(), 0);
        let mut sr = SearchResults::new();
        sr.request_id = Some("1234".to_string());
        let mb_result = mb.deliver_search_results(sr);
        assert!(mb_result.is_ok());
        assert_eq!(mb.search_results.len(), 1);
        let r = mb.get_search_results("1234");
        assert!(r.is_some());
    }
}
