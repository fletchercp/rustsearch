use std::sync::{Arc, RwLock};

use regex::Regex;
use hyper::server::{Request, Response};
use futures::Future;
use hyper;

use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;
use AppConfig;
pub type HttpHandler = fn(Request,
                          Arc<RwLock<NodeManager>>,
                          Arc<RwLock<Mailbox>>,
                          Arc<RwLock<AppConfig>>)
                          -> Box<Future<Item = Response, Error = hyper::Error>>;

#[derive(Clone)]
pub struct Router {
    routes: Vec<RouteHandler>,
}

#[derive(Clone)]
struct RouteHandler {
    re: Regex,
    pub handler: HttpHandler,
}

impl Router {
    pub fn new() -> Router {
        Router { routes: vec![] }
    }

    pub fn add_route(&mut self, re: &str, handler: HttpHandler) {
        let compiled_re = Regex::new(re).unwrap();
        let rh = RouteHandler {
            re: compiled_re,
            handler: handler,
        };
        self.routes.push(rh);
    }

    pub fn route(&self, path: &str) -> Option<HttpHandler> {
        for route in &self.routes {
            if route.re.is_match(path) {
                return Some(route.handler);
            }
        }
        None
    }
}
