use std::sync::{RwLock, Arc};

use hyper::server::{Request, Response};
use serde_json;
use futures::Future;
use futures;
use hyper;

use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;
use AppConfig;
use database::db::DBManager;

pub fn summary(
    _: Request,
    _: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    
    let v = DBManager::average_job_completion_time(&app_config).unwrap();
    let r = MetricsResponse{
        avg_job_latency: v,
    };
    let json = serde_json::to_string(&r).unwrap();
    Box::new(futures::future::ok(Response::new().with_body(json)))
}

#[derive(Serialize, Deserialize)]
pub struct MetricsResponse {
    avg_job_latency: f64,
}