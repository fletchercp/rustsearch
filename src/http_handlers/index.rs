use std::sync::{Arc, RwLock};
use rand;
use rand::Rng;
use time;
use regex::Regex;
use futures;

use serde_json;
use uuid::Uuid;
use rusqlite::Connection;
use hyper::{StatusCode, Method};

use hyper;
use futures::{Future, Stream};
use hyper::server::{Request, Response};

use cluster::node_manager::NodeManager;
use rpc::commands::CommandWrapper;
use database::db::DBManager;
use database::queries::*;
use cluster::shard::{Shard, Replica};
use inverted_index::document::Document;
use cluster::mailbox::Mailbox;
use AppConfig;
use inverted_index::index_sqlite::SQLiteIndex;
use inverted_index::index_store::Index;

pub fn handle_route_index(
    r: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    mailbox: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let re_static_route = Regex::new(r"/v1/api/index/(?P<index_name>.*)").unwrap();
    let p = r.path().to_string();
    let caps = re_static_route.captures(&p).unwrap();
    let _index_name = &caps["index_name"].to_string();
    match *r.method() {
        Method::Delete => delete_index(r, Arc::clone(&node_manager), Arc::clone(&mailbox), Arc::clone(&app_config)),
        Method::Put => create_index(r, Arc::clone(&node_manager), Arc::clone(&mailbox), Arc::clone(&app_config)),
        _ => {
            Box::new(futures::future::ok(
                Response::new().with_status(StatusCode::NotFound),
            ))
        }
    }
}

/// Handles a request to create an index
pub fn create_index(
    r: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let re_static_route = Regex::new(r"/v1/api/index/(?P<index_name>.*)").unwrap();
    let p = r.path().to_string();
    let caps = re_static_route.captures(&p).unwrap();
    let index_name = caps["index_name"].to_string();
    Box::new(
        r.body()
            .collect()
            .and_then(move |chunk| {
                let mut buffer: Vec<u8> = Vec::new();
                for i in chunk {
                    buffer.append(&mut i.to_vec());
                }
                Ok(buffer)
            })
            .map(move |buffer| {
                if buffer.is_empty() {
                    return Response::new().with_status(StatusCode::BadRequest);
                }
                if DBManager::index_exists(&index_name, &app_config.clone()).unwrap() {
                    return Response::new().with_status(StatusCode::BadRequest);
                }
                let index_request = serde_json::from_slice::<CreateIndexRequest>(&buffer).unwrap();
                // TODO: Need to see if this already exists?
                match DBManager::new_index(&index_name, &app_config.clone()) {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Unable to add new index: {:?}", e);
                        return Response::new().with_status(StatusCode::InternalServerError);
                    }
                };

                for _idx in 0..index_request.settings.index.number_of_shards {
                    let shard_id: String;
                    let wrapper = match node_manager.read() {
                        Ok(_nm) => {
                            shard_id = Uuid::new_v4().to_string();
                            CommandWrapper::new(&app_config.clone())
                                .command("create_index")
                                .index(index_name.to_string())
                                .shard(shard_id.clone())
                        }
                        Err(e) => {
                            println!("Unable to obtain read lock on node manager: {:?}", e);
                            return Response::new().with_status(StatusCode::InternalServerError);
                        }
                    };

                    match serde_json::to_string(&wrapper) {
                        Ok(json) => {
                            match node_manager.write() {
                                Ok(ref mut nm) => {
                                    let data_node = nm.random_data_node().unwrap();
                                    match data_node.send(json) {
                                        Ok(_) => {}
                                        Err(e) => {
                                            println!("Error sending to data node: {:?}", e);
                                        }
                                    };
                                    let new_shard = Shard {
                                        id: shard_id.clone(),
                                        index: index_name.to_string(),
                                        node: data_node.id.clone(),
                                    };
                                    let conn = Connection::open(
                                        app_config.read().unwrap().data_directory.clone() + "/cluster.db",
                                    ).unwrap();
                                    match new_shard.save(&conn) {
                                        Ok(_) => {
                                            println!(
                                                "Shard {:?} assigned to node {:?}:{:?}",
                                                wrapper.create_index,
                                                data_node.mgmt_host,
                                                data_node.mgmt_port
                                            );
                                        }
                                        Err(e) => {
                                            println!("Error saving shard: {:?}", e);
                                            return Response::new().with_status(StatusCode::InternalServerError);
                                        }
                                    }
                                }
                                Err(e) => {
                                    println!("Unable to obtain write lock on node manager: {:?}", e);
                                    return Response::new().with_status(StatusCode::InternalServerError);

                                }
                            };
                        }
                        Err(e) => {
                            println!("Error converting command to JSON: {:?}", e);
                            return Response::new().with_status(StatusCode::InternalServerError);

                        }
                    };

                    // Now we need to provision the appropriate number of replica shards for each primary shard
                    for _idx in 0..index_request.settings.index.number_of_replicas {
                        let replica_id: String;
                        let wrapper = match node_manager.read() {
                            Ok(_nm) => {
                                replica_id = Uuid::new_v4().to_string();
                                CommandWrapper::new(&app_config.clone())
                                    .command("create_replica")
                                    .index(index_name.to_string())
                                    .replica(replica_id.clone())
                            }
                            Err(e) => {
                                println!("Unable to obtain read lock on node manager: {:?}", e);
                                return Response::new().with_status(StatusCode::InternalServerError);

                            }
                        };

                        match serde_json::to_string(&wrapper) {
                            Ok(json) => {
                                match node_manager.write() {
                                    Ok(ref mut nm) => {
                                        let data_node = nm.random_data_node().unwrap();
                                        match data_node.send(json) {
                                            Ok(_) => {}
                                            Err(e) => {
                                                println!("Error sending to data node: {:?}", e);
                                            }
                                        };
                                        let new_replica = Replica {
                                            id: replica_id.clone(),
                                            index: index_name.to_string(),
                                            node: data_node.id.clone(),
                                            shard: shard_id.clone(),
                                        };
                                        let conn = Connection::open(
                                            app_config.read().unwrap().data_directory.clone() + "/cluster.db",
                                        ).unwrap();
                                        match new_replica.save(&conn) {
                                            Ok(_) => {
                                                println!(
                                                    "Replica {:?} assigned to node {:?}:{:?}",
                                                    wrapper.create_replica,
                                                    data_node.mgmt_host,
                                                    data_node.mgmt_port
                                                );
                                            }
                                            Err(e) => {
                                                println!("Error saving replica: {:?}", e);
                                                return Response::new().with_status(StatusCode::InternalServerError);

                                            }
                                        }
                                    }
                                    Err(e) => {
                                        println!("Unable to obtain write lock on node manager: {:?}", e);
                                        return Response::new().with_status(StatusCode::InternalServerError);

                                    }
                                };
                            }
                            Err(e) => {
                                println!("Error converting command to JSON: {:?}", e);
                                return Response::new().with_status(StatusCode::InternalServerError);
                            }
                        };
                    }
                }
                Response::new().with_status(StatusCode::Created)
            }),
    )
}

/// Handles a request to delete an index
pub fn delete_index(
    _: Request,
    _: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    _: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    Box::new(futures::future::ok(
        Response::new().with_status(StatusCode::NotImplemented),
    ))
}

/// Handles a request to list the shards for an index
pub fn shards(
    r: Request,
    _node_manager: Arc<RwLock<NodeManager>>,
    _mailbox: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let re_static_route = Regex::new(r"/v1/api/index/(?P<index_name>.*)/shards").unwrap();
    let caps = re_static_route.captures(r.path()).unwrap();
    let index_name = &caps["index_name"];
    match DBManager::shards_for_index(&app_config.clone(), &index_name.to_string()) {
        Ok(shards) => {
            Box::new(futures::future::ok(Response::new().with_body(
                serde_json::to_string(&shards).unwrap(),
            )))
        }
        Err(e) => {
            Box::new(futures::future::ok(
                Response::new().with_body(e.to_string()),
            ))
        }
    }
}

/// Handles a request to add a document to an index
pub fn document_request(
    r: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let start = time::Instant::now();
    let re_static_route = Regex::new(r"/v1/api/index/(?P<index_name>.+)/").unwrap();
    let p = r.path().to_string();
    let caps = re_static_route.captures(&p).unwrap();
    let index_name = caps["index_name"].to_string();
    Box::new(
        r.body()
            .collect()
            .and_then(move |chunk| {
                let mut buffer: Vec<u8> = Vec::new();
                for i in chunk {
                    buffer.append(&mut i.to_vec());
                }
                Ok(buffer)
            })
            .map(move |buffer| {                
                let document_request: AddDocumentRequest = match serde_json::from_slice(&buffer) {
                    Ok(result) => result,
                    Err(_) => {
                        return Response::new().with_status(StatusCode::BadRequest);
                    }
                };
                let new_document = Document::from_json(&document_request.content);
                let mut nm = node_manager.write().unwrap();
                match DBManager::shards_for_index(&app_config.clone(), &index_name) {
                    Ok(ref mut shards) => {
                        match rand::thread_rng().choose_mut(shards) {
                            Some(shard) => {
                                let command = CommandWrapper::new(&app_config.clone())
                                    .command("index_document")
                                    .document(new_document.clone())
                                    .index(index_name.to_string())
                                    .shard(shard.id.to_string());
                                let json = serde_json::to_string(&command).unwrap();
                                match nm.send_to_node(&shard.node, json) {
                                    Ok(_) => {
                                        // If we successfully sent off the write to the primary, now we need
                                        // to send it to each of the replicas
                                        let replicas = DBManager::replicas_for_shard(&app_config.clone(), &shard.id).unwrap();
                                        for replica in replicas {
                                            let command = CommandWrapper::new(&app_config.clone())
                                                .command("index_document")
                                                .document(new_document.clone())
                                                .index(index_name.to_string())
                                                .replica(replica.id.to_string());
                                            let json = serde_json::to_string(&command).unwrap();
                                            let _ = nm.send_to_node(&replica.node, json);
                                        }
                                    }
                                    Err(e) => {
                                        println!("There wasn an error sending index_document command to node: {:?}", e);
                                        return Response::new().with_status(StatusCode::InternalServerError);
                                    }
                                };

                            }
                            None => {
                                println!("No valid shard was found");
                                return Response::new().with_status(StatusCode::InternalServerError);
                            }
                        }
                    }
                    Err(e) => {
                        println!("There was an error getting shards for index: {:?}", e);
                        return Response::new().with_status(StatusCode::InternalServerError);
                    }
                }

                let finish = time::Instant::now();
                let took = finish.duration_since(start);
                let elapsed = took.as_secs() * 1000 + u64::from(took.subsec_nanos()) / 1_000_000;
                DBManager::insert_job_completion_time(elapsed as f64, &app_config.clone()).unwrap();
                Response::new().with_status(StatusCode::Ok)
            })
    )

}

/// Handles a request to search for a term in an index
pub fn search_index(r: Request, _node_manager: Arc<RwLock<NodeManager>>, _mailbox: Arc<RwLock<Mailbox>>, app_config: Arc<RwLock<AppConfig>>) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let re_static_route = Regex::new(r"/v1/api/index/(?P<index_name>.*)").unwrap();
    let p = r.path().to_string();
    let caps = re_static_route.captures(&p).unwrap();
    let index_name = caps["index_name"].to_string();
    Box::new(
        r.body()
            .collect()
            .and_then(move |chunk| {
                let mut buffer: Vec<u8> = Vec::new();
                for i in chunk {
                    buffer.append(&mut i.to_vec());
                }
                Ok(buffer)
            })
            .map(move |buffer| {
                let search_request: SearchRequest = match serde_json::from_slice(&buffer) {
                    Ok(result) => { result },
                    Err(_) => {
                        return Response::new().with_status(StatusCode::BadRequest);
                    }
                };
                let idx = SQLiteIndex::new(&index_name, &app_config.read().unwrap().data_directory).unwrap();
                let results = idx.occurrences_by_term(&search_request.term).unwrap();
                Response::new().with_body(serde_json::to_string(&results).unwrap())
            })
    )
}

/// Lists all the indices
pub fn list_indices(
    _r: Request,
    _node_manager: Arc<RwLock<NodeManager>>,
    _mailbox: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let index_db = DBManager::s_index_db(&app_config.clone()).unwrap();
    let mut stmt = index_db.prepare(QUERY_ALL_INDEXES).unwrap();
    let mut indices = Vec::<String>::new();
    let rows = stmt.query_map(&[], |row| row.get(0)).unwrap();

    for idx in rows {
        indices.push(idx.unwrap());
    }

    let result = serde_json::to_string(&indices).unwrap();
    Box::new(futures::future::ok(Response::new().with_body(
        serde_json::to_string(&result).unwrap(),
    )))
}

/// Adds a mapping for a field
pub fn _create_mapping(
    _r: Request,
    _node_manager: &Arc<RwLock<NodeManager>>,
    _mailbox: &Arc<RwLock<Mailbox>>,
    _app_config: &Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    Box::new(futures::future::ok(
        Response::new().with_status(StatusCode::NotImplemented),
    ))
}

/// Contains index-specific settings when making an index request
#[derive(Debug, Deserialize)]
pub struct IndexSettings {
    number_of_shards: i32,
    number_of_replicas: i32,
}

/// Settings for various requests that are included in those requests
#[derive(Debug, Deserialize)]
pub struct Settings {
    /// Index-specific settings
    index: IndexSettings,
}

/// Top level request wrapper for creating an index
#[derive(Debug, Deserialize)]
pub struct CreateIndexRequest {
    settings: Settings,
}

/// Data that accompanies a request to add a document to an index
#[derive(Debug, Deserialize)]
pub struct AddDocumentRequest {
    content: String,
}

/// Data that accopanies a request to search for a term in an index
#[derive(Debug, Deserialize)]
pub struct SearchRequest {
    term: String,
}
