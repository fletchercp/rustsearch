
use std::sync::{Arc, RwLock};


use futures;
use futures::future::Future;

use hyper;
use hyper::StatusCode;
use hyper::server::{Request, Response, Service};

use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;
use AppConfig;
use http_handlers::router::Router;

#[derive(Clone)]
pub struct RustSearch {
    pub node_manager: Arc<RwLock<NodeManager>>,
    pub mailbox: Arc<RwLock<Mailbox>>,
    pub app_config: Arc<RwLock<AppConfig>>,
    router: Router,
}

impl Service for RustSearch {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = Box<Future<Item = Self::Response, Error = Self::Error>>;

    fn call(&self, req: Request) -> Self::Future {
        match Router::route(&self.router, req.path()) {
            Some(rh) => {
                (rh)(
                    req,
                    Arc::clone(&self.node_manager),
                    Arc::clone(&self.mailbox),
                    Arc::clone(&self.app_config)
                )
            }
            _ => {
                Box::new(futures::future::ok(
                    Response::new().with_status(StatusCode::NotFound),
                ))
            }
        }
    }
}

impl RustSearch {
    pub fn new(router: Router, node_manager: Arc<RwLock<NodeManager>>, mailbox: Arc<RwLock<Mailbox>>, app_config: Arc<RwLock<AppConfig>>) -> RustSearch {
        RustSearch {
            node_manager: node_manager,
            mailbox: mailbox,
            app_config: app_config,
            router: router,
        }
    }
}

pub struct RequestArgs {
    req: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    mailbox: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
}

