use std::sync::{Arc, RwLock};

use hyper::server::{Request, Response};
use hyper;
use futures::Future;
use futures;

use cluster::node_manager::NodeManager;
use AppConfig;
use cluster::mailbox::Mailbox;

pub fn healthcheck(
    _: Request,
    _: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    _: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    Box::new(futures::future::ok(
        Response::new().with_status(hyper::StatusCode::Ok),
    ))
}
