use std::sync::{RwLock, Arc};

use hyper::server::{Request, Response};
use serde_json;
use futures::Future;
use futures;
use hyper;

use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;
use AppConfig;

pub fn list(
    _: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    _: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    match node_manager.write() {
        Ok(ref mut nm) => {
            match nm.refresh_nodes() {
                Ok(_) => {
                    let json = serde_json::to_string(&nm.nodes()).unwrap();
                    Box::new(futures::future::ok(Response::new().with_body(json)))
                }
                Err(_) => Box::new(futures::future::ok(Response::new().with_body("{}"))),
            }
        }
        Err(_) => Box::new(futures::future::ok(Response::new().with_body("{}"))),
    }
}
