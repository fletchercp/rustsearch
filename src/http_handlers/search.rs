/// Contains handlers for search related functionality

use std::sync::{Arc, RwLock};
use std::thread::sleep;
use time;

use serde_json;
use uuid::Uuid;

use rpc::commands::CommandWrapper;
use rpc::commands;
use database::db::DBManager;
use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;
use AppConfig;
use hyper::{Response, Request};
use futures::{Stream, Future};
use hyper;

/// Handles a request to search for a term in an index
pub fn search(
    r: Request,
    node_manager: Arc<RwLock<NodeManager>>,
    mailbox: Arc<RwLock<Mailbox>>,
    app_config: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    Box::new(
        r.body()
            .collect()
            .and_then(move |chunk| {
                let mut buffer: Vec<u8> = Vec::new();
                for i in chunk {
                    buffer.append(&mut i.to_vec());
                }
                Ok(buffer)
            })
            .map(move |buffer| {
                if buffer.is_empty() {
                    return Response::new().with_status(hyper::StatusCode::BadRequest);
                }
                let search = serde_json::from_slice::<SearchRequest>(&buffer).unwrap();
                // First we need to get all the nodes that have shards for this index
                let shards = DBManager::shards_for_index(&Arc::clone(&app_config), &search.index).unwrap();
                let mut shard_count = shards.len();
                let start = time::Instant::now();
                let mut search_result = Result::new();
                let request_id = Uuid::new_v4().to_string();
                let query: serde_json::Value = serde_json::from_str(&search.query).unwrap();

                {
                    let mut nm = node_manager.write().unwrap();
                    for shard in shards {
                        let command = CommandWrapper::new(&Arc::clone(&app_config))
                            .command("search")
                            .request_id(request_id.clone())
                            .index(search.index.to_string())
                            .shard(shard.id.to_string())
                            .query(query.to_string())
                            .to_json();

                        match nm.send_to_node(&shard.node, command) {
                            Ok(_) => {}
                            Err(e) => {
                                println!("Error sending search request to node: {:?}", e);
                            }
                        };
                    }
                }

                let one_hundred_millis = time::Duration::from_millis(1);
                let mut iterations = 100;
                let mut results = vec![];
                loop {
                    if iterations <= 0 {
                        break;
                    }
                    iterations -= 1;
                    sleep(one_hundred_millis);
                    {
                        let mut locked = mailbox.write().unwrap();
                        match locked.get_search_results(&request_id) {
                            Some(r) => {
                                results.push(r);
                                shard_count -= 1;
                                if shard_count == 0 {
                                    let finish = time::Instant::now();
                                    let took = finish.duration_since(start);
                                    let elapsed = took.as_secs() * 1000 + u64::from(took.subsec_nanos()) / 1_000_000;
                                    search_result.took = Some(elapsed);
                                    search_result.results = Some(results);
                                    println!(
                                        "All expected results received! Search took: {:?}ms",
                                        elapsed
                                    );
                                    return Response::new().with_body(serde_json::to_string(&search_result).unwrap());
                                }
                            }
                            None => {
                                continue;
                            }
                        }
                    }
                }
                Response::new().with_body("{}".to_string())
            })
        )
}

/// Search result and associated metadata returned to the user
#[derive(Serialize, Deserialize, Debug)]
struct Result {
    pub took: Option<u64>,
    pub results: Option<Vec<Vec<commands::SearchResults>>>,
}

impl Result {
    pub fn new() -> Result {
        Result {
            took: None,
            results: None,
        }
    }
}
/// Data that accopanies a request to search for a term in an index
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SearchRequest {
    index: String,
    query: String,
}
