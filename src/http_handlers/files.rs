use std::sync::{Arc, RwLock};
use std::path::Path;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use futures;
use futures::Future;
use hyper;
use hyper::StatusCode;
use hyper::server::{Request, Response};
use regex::Regex;

use AppConfig;
use cluster::node_manager::NodeManager;
use cluster::mailbox::Mailbox;

pub fn handle_route_static(
    r: Request,
    _: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    _: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let re_static_route = Regex::new(r"/static/(?P<file_path>.*)").unwrap();
    let caps = re_static_route.captures(r.path()).unwrap();
    let file_path = "static/".to_owned() + &caps["file_path"];
    let path = Path::new(&file_path);
    Box::new(futures::future::ok(match File::open(path) {
        Ok(f) => {
            let mut buf_reader = BufReader::new(f);
            let mut contents = String::new();
            buf_reader.read_to_string(&mut contents).unwrap();
            Response::new().with_body(contents)
        }
        Err(_) => Response::new().with_status(StatusCode::NotFound),
    }))
}

pub fn index(
    _: Request,
    _: Arc<RwLock<NodeManager>>,
    _: Arc<RwLock<Mailbox>>,
    _: Arc<RwLock<AppConfig>>,
) -> Box<Future<Item = Response, Error = hyper::Error>> {
    let path = Path::new("ui/build/index.html");
    Box::new(futures::future::ok(match File::open(path) {
        Ok(f) => {
            let mut buf_reader = BufReader::new(f);
            let mut contents = String::new();
            buf_reader.read_to_string(&mut contents).unwrap();
            Response::new().with_body(contents)
        }
        Err(e) => {
            println!("Error opening file: {:?} at {:?}", e, path);
            Response::new().with_status(StatusCode::NotFound)
        }
    }))
}
