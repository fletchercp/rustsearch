pub mod db;
pub mod queries;

use errors_chain::*;

#[derive(Debug, Clone, Copy)]
pub enum DataType {
    Null,
    Boolean,
    Float,
    Long,
    Date,
    Text,
}

impl DataType {
    pub fn into_string(self) -> Result<String> {
        match self {
            DataType::Null => { 
                Ok("null".to_string()) 
            },
            DataType::Boolean => {
                Ok("bool".to_string())
            },
            DataType::Float => {
                Ok("float".to_string())
            },
            DataType::Long => {
                Ok("long".to_string())
            },
            DataType::Date => {
                Ok("date".to_string())
            },
            DataType::Text => {
                Ok("text".to_string())
            }
        }
    }

    pub fn from_str(value: &str) -> Result<DataType> {
        match value.parse::<i64>() {
            Ok(_) => {
                return Ok(DataType::Long);
            },
            Err(_e) => {

            },
        };

        match value.parse::<f64>() {
            Ok(_) => {
                return Ok(DataType::Float);
            },
            Err(_e) => {

            },
        }

        if value == "true" || value == "false" {
            return Ok(DataType::Boolean);
        }

        if value == "" || value == "null" {
            return Ok(DataType::Null);
        }

        Ok(DataType::Text)
    }
}