use std::sync::{Arc, RwLock};
use std::time;
use rusqlite::Connection;
use errors_chain::*;
use database::queries::*;
use cluster::shard::{Shard, Replica};
use constants;

/// Provides a data structure to manage the connections to various `SQLite` databases
#[allow(dead_code)]
pub struct DBManager {
    cluster_db: Option<Connection>,
    index_db: Option<Connection>,
    metrics_db: Option<Connection>,
    app_config: Arc<RwLock<::AppConfig>>,
}

#[allow(dead_code)]
impl DBManager {
    /// Creates and returns a new DBManager
    pub fn new(app_config: Arc<RwLock<::AppConfig>>) -> Result<DBManager> {
        let mut dbm = DBManager {
            cluster_db: None,
            index_db: None,
            metrics_db: None,
            app_config: app_config,
        };
        dbm.initialize_dbs()?;
        Ok(dbm)
    }

    /// Returns a reference to the current cluster DB
    pub fn cluster_db(&mut self) -> Option<&Connection> {
        self.cluster_db.as_ref()
    }

    /// Returns a reference to the current index DB
    pub fn index_db(&mut self) -> Option<&Connection> {
        self.index_db.as_ref()
    }

    pub fn metrics_db(&mut self) -> Option<&Connection> {
        self.metrics_db.as_ref()
    }

    /// Static method that is used to get a SQLite connection to the index database
    /// Requires the caller to pass in the app_config struct
    pub fn s_index_db(app_config: &Arc<RwLock<::AppConfig>>) -> Result<Connection> {
        match app_config.read() {
            Ok(lock) => {
                Ok(Connection::open(
                    lock.data_directory.clone() +
                        constants::DEFAULT_CLUSTER_DB_NAME,
                ).map_err(|e| ErrorKind::ReadAppConfigError(e.to_string()))?)
            }
            Err(e) => Err(ErrorKind::ReadAppConfigError(e.to_string()).into()),
        }
    }

    /// Static method that is used to get a SQLite connection to the cluster database
    /// Requires the caller to pass in the app_config struct
    pub fn s_cluster_db(app_config: &Arc<RwLock<::AppConfig>>) -> Result<Connection> {
        match app_config.read() {
            Ok(lock) => {
                Ok(Connection::open(
                    lock.data_directory.clone() +
                        constants::DEFAULT_CLUSTER_DB_NAME,
                ).map_err(|e| ErrorKind::ReadAppConfigError(e.to_string()))?)
            }
            Err(e) => Err(ErrorKind::ReadAppConfigError(e.to_string()).into()),
        }
    }

    pub fn s_metrics_db(app_config: &Arc<RwLock<::AppConfig>>) -> Result<Connection> {
        match app_config.read() {
            Ok(lock) => {
                Ok(Connection::open(
                    lock.data_directory.clone() +
                        constants::DEFAULT_METRICS_DB_NAME,
                ).map_err(|e| ErrorKind::ReadAppConfigError(e.to_string()))?)
            }
            Err(e) => Err(ErrorKind::ReadAppConfigError(e.to_string()).into()),
        }
    }

    /// Gets all of the shards for an Index
    pub fn shards_for_index(app_config: &Arc<RwLock<::AppConfig>>, index: &str) -> Result<Vec<Shard>> {
        let mut shards: Vec<Shard> = Vec::new();
        let conn = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            }
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };
        let mut stmt = conn.prepare(QUERY_GET_SHARDS_BY_INDEX).map_err(|e| {
            ErrorKind::ReadAppConfigError(e.to_string())
        })?;
        let iter = stmt.query_map(&[&index], |row| {
            Shard {
                id: row.get(0),
                node: row.get(1),
                index: index.to_string(),
            }
        }).unwrap();

        for shard in iter {
            match shard {
                Ok(s) => {
                    shards.push(s);
                }
                Err(e) => {
                    println!("Error pushing shard result: {:?}", e);
                }
            };
        }
        Ok(shards)
    }

    /// Gets all of the Replicas for a Shard
    pub fn replicas_for_shard(app_config: &Arc<RwLock<::AppConfig>>, shard: &str) -> Result<Vec<Replica>> {
        let mut replicas: Vec<Replica> = Vec::new();
        let conn = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            }
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };
        let mut stmt = conn.prepare(QUERY_GET_REPLICAS_BY_SHARD).map_err(|e| {
            ErrorKind::ReadAppConfigError(e.to_string())
        })?;
        let iter = stmt.query_map(&[&shard], |row| {
            Replica {
                id: row.get(0),
                index: row.get(1),
                node: row.get(2),
                shard: shard.to_string(),
            }
        }).unwrap();

        for replica in iter {
            match replica {
                Ok(s) => {
                    replicas.push(s);
                }
                Err(e) => {
                    println!("Error pushing replica result: {:?}", e);
                }
            };
        }
        Ok(replicas)
    }

    /// Creates a new Index and adds it to the list of databases
    pub fn new_index(name: &str, app_config: &Arc<RwLock<::AppConfig>>) -> Result<()> {
        let index_db = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            }
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };

        index_db.execute(QUERY_INSERT_INDEX, &[&name]).chain_err(
            || {
                ErrorKind::SQLQueryFailed(QUERY_INSERT_INDEX.to_string())
            },
        )?;
        Ok(())
    }

    pub fn index_exists(name: &str, app_config: &Arc<RwLock<::AppConfig>>) -> Result<bool> {
        let index_db = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            }
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };

        let mut stmt = index_db.prepare(QUERY_INDEX_EXISTS).map_err(|e| {
            ErrorKind::ReadAppConfigError(e.to_string())
        })?;
        let mut count = 0;
        let iter = stmt.query_map(&[&name], |_| {

        }).unwrap();

        for _replica in iter {
            count += 1
        }

        if count > 0 {
            return Ok(true);
        }
        Ok(false)

    }

    pub fn average_job_completion_time(app_config: &Arc<RwLock<::AppConfig>>) -> Result<f64> {
        let db = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_METRICS_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            },
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };

        Ok(db.query_row(QUERY_JOB_AVG_COMPLETION_TIME, &[], |row| {
            row.get(0)
        }).unwrap())
    }

    pub fn insert_job_completion_time(value: f64, app_config: &Arc<RwLock<::AppConfig>>) -> Result<()> {
        let db = match app_config.read() {
            Ok(lock) => {
                match Connection::open(
                    lock.data_directory.clone() + constants::DEFAULT_METRICS_DB_NAME,
                ) {
                    Ok(conn) => conn,
                    Err(e) => {
                        return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
                    }
                }
            },
            Err(e) => {
                return Err(ErrorKind::ReadAppConfigError(e.to_string()).into());
            }
        };
          
        let now = time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap();
        db.execute(QUERY_INSERT_METRIC, &[&"job_completion_time".to_string(), &value, &now.as_secs().to_string()]).chain_err(
            || {
                    ErrorKind::SQLQueryFailed(QUERY_JOB_AVG_COMPLETION_TIME.to_string())
                },
            )?;
        Ok(())
    }

    /// Initializes the cluster and index databases
    fn initialize_dbs(&mut self) -> Result<()> {
        if self.cluster_db.is_none() {
            match Connection::open(
                self.app_config.read().unwrap().data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
            ) {
                Ok(db) => {
                    self.cluster_db = Some(db);
                }
                Err(_) => {
                    return Err(
                        ErrorKind::DatabaseError(constants::DEFAULT_CLUSTER_DB_NAME.to_string()).into(),
                    );
                }
            }
        }

        if self.index_db.is_none() {
            match Connection::open(
                self.app_config.read().unwrap().data_directory.clone() + constants::DEFAULT_CLUSTER_DB_NAME,
            ) {
                Ok(db) => {
                    self.index_db = Some(db);
                }
                Err(_) => {
                    return Err(
                        ErrorKind::DatabaseError(constants::DEFAULT_CLUSTER_DB_NAME.to_string()).into(),
                    );
                }
            }
        }

        if self.metrics_db.is_none() {
            match Connection::open(
                self.app_config.read().unwrap().data_directory.clone() + constants::DEFAULT_METRICS_DB_NAME,
            ) {
                Ok(db) => {
                    db.execute(QUERY_CREATE_METRICS_TABLE, &[])
                    .chain_err(|| {
                        ErrorKind::SQLQueryFailed(QUERY_CREATE_METRICS_TABLE.to_string())
                    })?;
                    self.metrics_db = Some(db);
                }
                Err(_) => {
                    return Err(
                        ErrorKind::DatabaseError(constants::DEFAULT_METRICS_DB_NAME.to_string()).into(),
                    );
                }
            }
        }
        Ok(())
    }
}