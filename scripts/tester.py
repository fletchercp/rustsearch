#!/usr/bin/python

import json
import time
import requests
import grequests
import argparse
from faker import Faker


ES_URL = "http://localhost:3000/v1/api"
fake = Faker()
async_list = []
session = requests.Session()

def random_user_document():
    """
    This generates a random JSON document
    """
    schema = {
        "content": {
            "name": fake.name(),
            "address": fake.address(),
            "biography": fake.text(),
            "misc": {
                "job": fake.job(),
            }
        }
    }
    return json.dumps(schema)

def create_index(args):
    schema = {
        "settings": {
            "index": {
                "number_of_shards": args.p,
                "number_of_replicas": args.r
            },
        },
        "refresh": True
    }

    body = json.dumps(schema)
    headers = {"Content-Type":"application/json"}
    url = "%s/index/%s" % (ES_URL, args.idx_name)
    r = requests.put(url, data=body, headers=headers)
    print r.content

def list_indices(args):
    url = "%s/indices" % (ES_URL)
    headers = {"Content-Type":"application/json"}
    r = requests.get(url, headers=headers)
    print r.content

def list_nodes(args):
    url = "%s/nodes" % (ES_URL)
    headers = {"Content-Type":"application/json"}
    r = requests.get(url, headers=headers)
    print r.content

def list_shards(args):
    url = "%s/index/%s/shards" % (ES_URL, args.idx_name)
    headers = {"Content-Type":"application/json"}
    r = requests.get(url, headers=headers)
    print r.content

def add_existing_document(index, content):
    schema = {
        "content": content
    }
    body = json.dumps(schema)
    url = "%s/index/%s/document" % (ES_URL, index)
    headers = {"Content-Type":"application/json"}
    action_item = grequests.put(url, stream=False, session=session, headers=headers, data=body)
    async_list.append(action_item)

def add_document(args):
    schema = {
        "content": args.d
    }
    body = json.dumps(schema)
    url = "%s/index/%s/document" % (ES_URL, args.idx_name)
    headers = {"Content-Type":"application/json"}
    r = requests.put(url, headers=headers, data=body)
    print r.content

def bulk_doc(args):
    fh = open(args.f, 'r')
    lines = fh.readlines()
    fh.close()
    url = "%s/index/%s/document" % (ES_URL, args.idx_name)
    headers = {"Content-Type":"application/json"}
    print "Adding documents in bulk..."
    for line in lines:
        schema = {
            "content": line
        }
        body = json.dumps(schema)
        action_item = grequests.put(url, stream=False, session=session, headers=headers, data=body)
        async_list.append(action_item)
    grequests.map(async_list, size=1000)
    print "Done!"

def search(args):
    url = "%s/search" % (ES_URL)
    headers = {"Content-Type":"application/json"}
    schema = {
        "index": args.idx_name,
        "query": args.query
    }
    body = json.dumps(schema)
    r = requests.put(url, headers=headers, data=body)
    print r.content

def load_random_documents(args):
    for c in xrange(0, args.total):
        add_existing_document(args.idx_name, random_user_document())
    print "Beginning async..."
    grequests.map(async_list, size=200)

def load_shakespeare(args):
    """
    Loads the sample shakespeare works
    """
    fh = open('shakespeare.json', 'r')
    for line in fh.readlines():
        add_existing_document("shakespeare", line)

    grequests.map(async_list, size=200)


parser = argparse.ArgumentParser(description='Interact with RusticSearch!')
subparsers = parser.add_subparsers(help='sub-command help')

p_create_idx = subparsers.add_parser("create-index", help="creates a new index")
p_create_idx.add_argument("idx_name", help="Name of index to create")
p_create_idx.add_argument("-p", help="Number of primary shards", type=int, default=1)
p_create_idx.add_argument("-r", help="Number of replica shards", type=int, default=1)
p_create_idx.set_defaults(func=create_index)

p_list_indices = subparsers.add_parser("list-indices", help="lists all indices")
p_list_indices.set_defaults(func=list_indices)

p_list_nodes = subparsers.add_parser("list-nodes", help="lists all nodes")
p_list_nodes.set_defaults(func=list_nodes)

p_list_shards = subparsers.add_parser("list-shards", help="lists all shards for an index")
p_list_shards.add_argument("idx_name", help="Name of the index to add the document to")
p_list_shards.set_defaults(func=list_shards)

p_add_doc = subparsers.add_parser("add-document", help="adds a document to an index")
p_add_doc.add_argument("idx_name", help="Name of the index to add the document to")
p_add_doc.add_argument("-f", help="Path to the file that contains the document to add")
p_add_doc.add_argument("-d", help="String literal that is the body of the document", required=True)
p_add_doc.set_defaults(func=add_document)

p_bulk_doc = subparsers.add_parser("bulk-doc", help="adds a bunch of documents from JSON in a file")
p_bulk_doc.add_argument("idx_name", help="Name of the index to add the documents to")
p_bulk_doc.add_argument("-f", help="Path to file containing the documents", required=True)
p_bulk_doc.set_defaults(func=bulk_doc)

p_search = subparsers.add_parser("search", help="searches an index with a query")
p_search.add_argument("idx_name", help="Name of the index to search", type=str)
p_search.add_argument("query", help="Query for which to search", type=str)
p_search.set_defaults(func=search)

p_rand_docs = subparsers.add_parser("random-docs", help="generates and loads random documents")
p_rand_docs.add_argument("idx_name", help="Name of the index to add docs to", type=str)
p_rand_docs.add_argument("total", help="Total number of random documents to add", type=int)
p_rand_docs.set_defaults(func=load_random_documents)

args = parser.parse_args()
args.func(args)
